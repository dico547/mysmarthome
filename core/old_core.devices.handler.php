<?php
	header('Content-Type: application/json');

	require_once( dirname(__FILE__) . '/../core.php' );


	/* Get parameters
	--------------------------------------------------------------------------- */
	if (isset($_GET['id'])) $getID = clean($_GET['id']);
	if (isset($_GET['action'])) $action = clean($_GET['action']);



	$obj = new devices;



	/* Device log/monitor
	--------------------------------------------------------------------------- */
	if ($action == "deviceLog") {

		$deviceIntID = clean($_POST['deviceIntID']);
		$value = clean($_POST['value']);

		$result = $obj->setDeviceLog($deviceIntID, $value);
		echo json_encode($result);
	}


	/* Device set public
	--------------------------------------------------------------------------- */
	if ($action == "devicePublic") {

		$deviceIntID = clean($_POST['deviceIntID']);
		$value = clean($_POST['value']);

		$result = $obj->setDevicePublic($deviceIntID, $value);
		echo json_encode($result);
	}



	/* Device dashboard
	--------------------------------------------------------------------------- */
	if ($action == "deviceDashboard") {

		$deviceIntID = clean($_POST['deviceIntID']);
		$value = clean($_POST['value']);

		$result = $obj->setDeviceDashboard($deviceIntID, $value);
		echo json_encode($result);
	}

?>