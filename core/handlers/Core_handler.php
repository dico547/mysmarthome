<?php
	require_once( dirname(__FILE__) . '/../../core.php' );
	


	/**
	* do Login
	* Gets form input and run login process
	* 
  	* @uses Msh\Auth
  	* @author Robert Andresen <ra@fosen-utvikling.no>
	*/
	if ($_GET['action'] == 'saveUserModule')
	{
		$modules = $_POST['modules'];

		$result = $objCore->setUserModules($modules);

		if ($result['status'] == 'success') {
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		} else {
			echo '<pre>';
				print_r($result);
			echo '</pre>';
		}

		exit();		
	}




	if ($_GET['action'] == 'moveUserModule')
	{
		$getModule = $_GET['module'];
		$getNewRang = $_GET['rang'];

		$result = $objCore->setUserModuleRang($getModule, $getNewRang);

		if ($result['status'] == 'success') {
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		} else {
			echo '<pre>';
				print_r($result);
			echo '</pre>';
		}

		exit();	
	}


?>