<?php
	require_once( dirname(__FILE__) . '/../core.php' );

	if (isset($_SESSION['MSH_USER_AUTH'])) {
		$_SESSION['MSH_USER_AUTH'] = $_SESSION['MSH_USER_AUTH'];
	} else {
		header('Location: '.URL.'login/');
		exit();
	}

?>