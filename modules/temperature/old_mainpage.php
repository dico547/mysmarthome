<h1><?php echo _('Climate'); ?></h1>

<ol class="breadcrumb">
	<li><a href="index.php"><i class="fa fa-home"></i> <?php echo _('Home'); ?></a></li>
	<li class="active"><?php echo _('Climate'); ?></li>
</ol>


<?php
	$p = array (
		'categories' => array(2,3),
	);

	$getDevices = $objDevices->getDevices($p);
?>

<?php foreach ($getDevices as $intID => $dData): ?>
	<div class="device-box">
		<div class="device-box-inner">

			<!-- <div class="dropdown">
				<div class="device-box-title dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
					<?php echo $dData['name']; ?>
				</div>
				<ul class="dropdown-menu" role="menu">
					<li><a href="?m=settings"><i class="fa fa-fw fa-bar-chart"></i> <?php echo _('View chart'); ?></a></li>

					<?php if ($dData['monitor'] == 0): ?>
						<li><a href="?m=settings"><i class="fa fa-fw fa-stop"></i> <?php echo _('Monitoring is OFF'); ?></a></li>
					<?php else: ?>
						<li><a href="?m=settings"><i class="fa fa-fw fa-play"></i> <?php echo _('Monitoring is ON'); ?></a></li>
					<?php endif; ?>

					<?php if ($dData['public'] == 0): ?>
						<li><a href="?m=settings"><i class="fa fa-fw fa-stop"></i> <?php echo _('Not in public'); ?></a></li>
					<?php else: ?>
						<li><a href="?m=settings"><i class="fa fa-fw fa-group"></i> <?php echo _('Public'); ?></a></li>
					<?php endif; ?>
				</ul>
			</div> -->


			<div class="device-box-title">
				<a href="?m=temperature&page=sensor&id=<?php echo $dData['deviceIntID']; ?>">
					<?php echo $dData['name']; ?>
				</a>
			</div>


			<div>
				<?php
					$c = 1;
					foreach ($dData['last_values'] as $unitID => $uData) {
						if (!empty($uData['value'])) {
							echo '<div class="device-box-value'.$c.'">';
								echo $uData['value'] . ' ' . $uData['unit']['tag'];
							echo '</div>';

							$c++;
						}
					}
				?>
			</div>


			<div class="device-box-time">
				<?php
					if ((time() - $dData['last_values'][1]['time']['timestamp']) > 1800) {
						echo '<span style="color:red;"><i class="fa fa-warning"></i> '.ago($dData['last_values'][1]['time']['timestamp']).'</span>';
					}
					else {
						echo '<span>'.ago($dData['last_values'][1]['time']['timestamp']).'</span>';
					}
				?>
				
			</div>

			<div class="device-box-tools">
				<?php
					if ($dData['monitor'] == 0) {
						$monitor_class = 'btn-default';
						$monitor_new_value = 1;
					}
					else {
						$monitor_class = 'btn-success';
						$monitor_new_value = 0;
					}

					if ($dData['public'] == 0) {
						$public_class = 'btn-default';
						$public_new_value = 1;
					}
					else {
						$public_class = 'btn-success';
						$public_new_value = 0;
					}

					if ($dData['dashboard'] == 0) {
						$dashboard_class = 'btn-default';
						$dashboard_new_value = 1;
					}
					else {
						$dashboard_class = 'btn-success';
						$dashboard_new_value = 0;
					}

					//echo '<a href="?m=climate&page=sensor&id='.$dData['deviceIntID'].'" style="margin-right:5px;" class="btn btn-default"><i class="fa fa-info-circle"></i></a>';
					echo '<a href="?m=temperature&page=chart&devices='.$dData['deviceIntID'].'" style="margin-right:5px;" class="btn btn-default"><i class="fa fa-bar-chart"></i></a>';
					echo '<a href="core/handlers/Devices_handler.php?action=setMonitor&id='.$dData['deviceIntID'].'&value='.$monitor_new_value.'" style="margin-right:5px;" class="btn '.$monitor_class.'"><i class="fa fa-play"></i></a>';
					echo '<a href="core/handlers/Devices_handler.php?action=setPublic&id='.$dData['deviceIntID'].'&value='.$public_new_value.'" style="margin-right:5px;" class="btn '.$public_class.'"><i class="fa fa-group"></i></a>';
					echo '<a href="core/handlers/Devices_handler.php?action=setDashboard&id='.$dData['deviceIntID'].'&value='.$dashboard_new_value.'" style="margin-right:5px;" class="btn '.$dashboard_class.'"><i class="fa fa-desktop"></i></a>';
				?>
			</div>
		</div>
	</div>
<?php endforeach; ?>



<?php
	echo '<pre>';
		print_r($getDevices);
	echo '</pre>';


/*	
	echo "<div id='' class='list'>";
		$query = "SELECT * 
				  FROM fu_data_devices 
				  WHERE user_id='{$user['user_id']}'
				  	AND (category LIKE 'climate')
					AND deactive='0'
				  ORDER BY device_name ASC
				  ";
		$result = $mysqli->query($query);
		$numRows = $result->num_rows;


		echo "<table class='table'>";

			while ($row = $result->fetch_array()) {
				//if (!empty($row['device_name'])) {
					$deviceData = device_getLastValue($row['device_int_id']);

					if ((time() - $deviceData['time']) > 1800) {
						$syncError = true;
					} else {
						$syncError = false;
					}


					echo "<tr>";

						// Device/sensor name
						echo "<td>";
							if ($syncError) {
								echo "<i style='color:red; margin-right:8px;' class='fa fa-exclamation-triangle fa-fw showTooltip' title='"._('Sync error?')." "._('Last sync: ') . ago($deviceData['time']) . "'></i>";
							}

							echo "<a href='?m=climate&page=sensor&id={$row['device_int_id']}'>";
								if (!empty($row['device_name'])) echo $row['device_name'];
								else echo "<i>" . _('Unnamed sensor') . "</i>";
							echo "</a>";

						echo "</td>";


						// Value 1
						echo "<td>";
							if (!empty($deviceData['value1'])) echo $deviceData['value1'] . "&deg;";
						echo "</td>";


						// Value 2
						echo "<td>";
							if (!empty($deviceData['value2'])) echo $deviceData['value2'] . "%";
						echo "</td>";

						// Time ago
						echo "<td width='170px' class='visible-lg visible-md'>";
							if ($syncError)	$errorColor = "orange;";
							else $errorColor = "";

							echo "<span style='color:$errorColor;'>" . ago($deviceData['time']) . "</span>";
						echo "</td>";


						// Settings
						echo "<td class='visible-lg visible-md'>";
							if ($row['public'] == 1) echo "<i style='margin-right:8px;' class=\"fa fa-cloud toolTip\" title=\""._('This device is avalible on the public page')."\"></i>";
							if ($row['monitor'] == 1) echo "<i style='margin-right:8px;' class=\"fa  fa-file-text-o toolTip\" title=\""._('This device logs to the local database')."\"></i>";
							if ($row['dashboard'] == 1) echo "<i style='margin-right:8px;' class=\"fa  fa-desktop toolTip\" title=\""._('This device is on the dashboard')."\"></i>";
						echo "</td>";

					echo "</tr>";
				//}
			}

		echo "</table>";
	echo "</div>";*/

?>