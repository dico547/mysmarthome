<?php
	require_once( dirname(__FILE__) . '/../../core.php' );


	if (isset($_GET['debug'])) {
		$debug = true;
	}

	if ($debug) {
		$starttime = explode(' ', microtime());
		$starttime = $starttime[1] + $starttime[0];
	}



	$timeFrom = $config['tmp_sync_limit'];
	if ($debug) echo "DB Time from: " . date('d-m-Y H:i:s', $timeFrom) . "<br />";

	$c = 1;

	$timeFrom2 = ( $timeFrom - (3600 * 3) );
	if ($debug) echo "New Time from: " . date('d-m-Y H:i:s', $timeFrom2) . "<br />";

	$timeTo = $timeFrom;

	if ($debug) echo "Between <br />" . date('d-m-Y H:i:s', $timeFrom2) . ' and <br />'. date('d-m-Y H:i:s', $timeTo) . '<br />';

	
	//$result->close();



	/*echo "syncStart: $syncStart <br />";
	echo "syncStartNext: $syncStartNext <br />";
	echo "syncNum: $syncNum <br /><br />";*/


	// GET device units
	$query = "SELECT device_int_id, value_unit, value2_unit, value3_unit FROM msh_devices";
	$result = $mysqli->query($query);
	//$numRows = $result->num_rows;

	$deviceUnits = array();
	while ($row = $result->fetch_array()) {
		$deviceUnits[$row['device_int_id']]['value_unit'] = $row['value_unit'];
		$deviceUnits[$row['device_int_id']]['value2_unit'] = $row['value2_unit'];
		$deviceUnits[$row['device_int_id']]['value3_unit'] = $row['value3_unit'];
	}

	//$result->close();

	/*echo "<pre>";
		print_r($deviceUnits);
	echo "</pre>";*/
	

	
	$query = "SELECT 
				log.device_int_id, 
				log.time, 
				log.value, 
				log.value2, 
				log.value3
			  FROM msh_data_log AS log
			  WHERE log.time BETWEEN $timeFrom2 AND $timeTo";
	if ($debug) echo "Query: $query <br /><br />";

	$result = $mysqli->query($query);
	//$numRows = $result->num_rows;

	while ($row = $result->fetch_array()) {

		/*echo "{$row['device_name']}<br />";
		echo "{$row['time']}<br />";
		echo "{$row['value']} - {$row['value_unit']}<br />";
		echo "{$row['value2']} - {$row['value2_unit']}<br />";
		echo "{$row['value3']} - {$row['value3_unit']}<br />";*/
		$insert = false;

		// VALUE 1
		if (!empty($deviceUnits[$row['device_int_id']]['value_unit'])) {
			if ($debug) echo "Value 1: <br />";
			$p = array (
				'device_int_id' => $row['device_int_id'],
				'time' => $row['time'],
				'unit_id' => $deviceUnits[$row['device_int_id']]['value_unit'],
				'value' => $row['value'],
			);

			$addLogResult = $objDevices->addLog($p);
			if ($debug) {
				echo "<pre>";
					print_r($addLogResult);
				echo "</pre>";
			}

			$insert = true;
		}

		// VALUE 2
		if (!empty($deviceUnits[$row['device_int_id']]['value2_unit'])) {
			if ($debug) echo "Value 2: <br />";
			$p = array (
				'device_int_id' => $row['device_int_id'],
				'time' => $row['time'],
				'unit_id' => $deviceUnits[$row['device_int_id']]['value2_unit'],
				'value' => $row['value2'],
			);

			$addLogResult = $objDevices->addLog($p);
			if ($debug) {
				echo "<pre>";
					print_r($addLogResult);
				echo "</pre>";
			}
			
			$insert = true;
		}

		// VALUE 3
		if (!empty($deviceUnits[$row['device_int_id']]['value3_unit'])) {
			if ($debug) echo "Value 3: <br />";
			$p = array (
				'device_int_id' => $row['device_int_id'],
				'time' => $row['time'],
				'unit_id' => $deviceUnits[$row['device_int_id']]['value3_unit'],
				'value' => $row['value3'],
			);

			$addLogResult = $objDevices->addLog($p);
			if ($debug) {
				echo "<pre>";
					print_r($addLogResult);
				echo "</pre>";
			}

			$insert = true;
		}


		if (!$insert && $debug) {
			echo "This is not inserted:<br />";
			echo "{$row['device_name']}<br />";
			echo "{$row['time']}<br />";
			echo "{$row['value']} - {$row['value_unit']}<br />";
			echo "{$row['value2']} - {$row['value2_unit']}<br />";
			echo "{$row['value3']} - {$row['value3_unit']}<br />";
		}

		if ($debug) {
			echo "<hr />";
			$c++;
		}

	}

	//$result->close();


	$query = "UPDATE msh_config SET 
				config_value='".$timeFrom2."'
				WHERE config_name LIKE 'tmp_sync_limit'";
	$result = $mysqli->query($query);

	


	if ($debug) {
		echo "<br /><br />";

		echo "Found <b>$c</b> records!";

		echo "<br /><br />";

		$mtime = explode(' ', microtime());
		$totaltime = $mtime[0] + $mtime[1] - $starttime;
		printf('Page loaded in %.3f seconds.', $totaltime);
		echo '<br />';
	}

	$result = mysqli_close($mysqli);

	if ($debug) {
		echo "Close mysqli <br />";
		echo "<pre>";
			print_r($result);
		echo "</pre>";
	}
	


?>