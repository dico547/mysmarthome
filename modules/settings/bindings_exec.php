<?php	
	require_once( dirname(__FILE__) . '/../../core.php' );


	/* Get parameters
	--------------------------------------------------------------------------- */
	if (isset($_GET['id'])) $getID = clean($_GET['id']);
	if (isset($_GET['action'])) $action = clean($_GET['action']);



	$bindingPath = ABSPATH . "bindings/";






	
	if ($action == "uploadBinding") {

		$uploadPath = ABSPATH . "bindings/";
		//$uploadPath = "../bindings/";
		$allowedUploads = array("zip", "rar");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);

		if (($_FILES["file"]["size"] < 20000) && in_array($extension, $allowedUploads)) {
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
			} 

			else {
				echo "uploadPath: " . $uploadPath . "<br>";
				echo "Upload: " . $_FILES["file"]["name"] . "<br>";
				echo "Type: " . $_FILES["file"]["type"] . "<br>";
				echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
				echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
				if (file_exists($uploadPath . $_FILES["file"]["name"])) {
					echo $_FILES["file"]["name"] . " already exists. ";
				} else {
					move_uploaded_file($_FILES["file"]["tmp_name"], $uploadPath . $_FILES["file"]["name"]);
					chmod($uploadPath . $_FILES["file"]["name"], 0777);
					echo "Stored in: " . $uploadPath . $_FILES["file"]["name"];
				}
			}
		} else {
			echo "Invalid file";
		}
	}






	if ($action == "install") {

		$getBinding = clean($_GET['binding']);

		echo "getBinding: $getBinding <br />";

		

		//$tmpFolder = '_tmp_' . time();
		$tmpFolder = '_tmp/';
		if (!file_exists($bindingPath . $tmpFolder)) {
			mkdir($bindingPath . $tmpFolder, 0777);
		}


		$zip = new ZipArchive;
		$res = $zip->open($bindingPath . $getBinding);
		if ($res === TRUE) {
			//$zip->extractTo($tmpFolder);
			$zip->extractTo("../../bindings/_tmp/");
			$zip->close();
			echo 'woot!';
		} else {
			echo 'doh!';
		}



		$xml = simplexml_load_file($bindingPath . $tmpFolder . 'binding.xml');

		echo "<pre>";
			print_r($xml);
		echo "</pre>";


		

		if (!file_exists($bindingPath . $xml->foldername)) {
			mkdir($bindingPath . $xml->foldername, 0777);
		}
		

		$fromFolder = $bindingPath . $tmpFolder;
		$toFolder = $bindingPath . $xml->foldername . '/';

		echo "fromFolder: $fromFolder <br />";
		echo "toFolder: $toFolder <br />";

		copyFilesInDirectory($fromFolder, $toFolder);


		include($toFolder . 'install.php');



		//deleteAllFilesAndDir($bindingPath . 'tmp/');
		//deleteAllFilesAndDir($bindingPath . '_tmp/');
		//unlink($bindingPath . 'template.bindingbinding.xml');
		//unlink($bindingPath . 'template.bindingindex.php');
		//unlink($bindingPath . 'template.bindinginstall.php');
		
		unlink($bindingPath . $getBinding);

		//deleteAllFilesAndDir($bindingPath . $tmpFolder);
	}




	if ($action == "deletePackage") {
		$getBinding = clean($_GET['binding']);

		echo "getBinding: $getBinding <br />";
		echo "File: ".$bindingPath . $getBinding." <br />";

		unlink($bindingPath . $getBinding);
	}



	if ($action == "deleteBinding") {
	}
	

?>