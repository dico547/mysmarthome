<?php
	if (isset($_GET['ajax']) && $_GET['ajax'] == true) {
		require_once( dirname(__FILE__) . '/../../core.php' );
	}
?>



<h1><?php echo _('Activity log'); ?></h1>

<ol class="breadcrumb">
	<li><a href="index.php"><i class="fa fa-home"></i> <?php echo _('Home'); ?></a></li>
	<li><a href="?m=settings"><i class="fa fa-cog"></i> <?php echo _('Settings'); ?></a></li>
	<li class="active"><?php echo _('Activity log'); ?></li>
</ol>


<?php

	// Set all activity as confirmed
	$result = $objCore->confirmActivity();

	
	$query = "SELECT * FROM msh_activity_log WHERE user_id='{$thisUser['user_id']}' ORDER BY time DESC";
	$result = $mysqli->query($query);
	$numRows = $result->num_rows;

	if ($numRows > 0) {
		echo "<table class='table table-hover table-striped'>";
			while ($row = $result->fetch_array()) {
				echo "<tr>";
					echo "<td>".date('d-m-Y H:i:s', $row['time'])."</td>";
					echo "<td>".ago($row['time'])."</td>";
					echo "<td>{$row['message']}</td>";
				echo "</tr>";
			}
		echo "</table>";
	}
	else {
		echo "<div class='noResult'>"._('No records found')."</div>";
	}

?>