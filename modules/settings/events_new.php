<h1><?php echo _('New event'); ?></h1>

<form>

	<div class="form-group">
		<label for="inputTitle"><?php echo _('Title'); ?></label>
		<input type="text" class="form-control" id="inputTitle" name="inputTitle" placeholder="<?php echo _('Event title'); ?>">
	</div>

	<div class="form-group">
		<label for="inputTitle"><?php echo _('Date') . ' ' . _('From') . '->' . _('To'); ?></label><br />
		<input type="text" class="form-control datepicker" style="display:inline-block; max-width:150px;" id="inputDateFrom" name="inputDateFrom" placeholder="<?php echo _('dd-mm-YYYY'); ?>"> ->
		<input type="text" class="form-control datepicker" style="display:inline-block; max-width:150px;" id="inputDateTo" name="inputDateTo" placeholder="<?php echo _('dd-mm-YYYY'); ?>">

		<div>
			Leave blank to not filter between dates...
		</div>
	</div>

	<div class="form-group">
		<label for="inputTitle"><?php echo _('Time') . ' ' . _('From') . '->' . _('To'); ?></label><br />
		<input type="text" class="form-control datepicker" style="display:inline-block; max-width:65px;" id="inputClockFrom" name="inputClockFrom" placeholder="<?php echo _('HH:mm'); ?>"> ->
		<input type="text" class="form-control datepicker" style="display:inline-block; max-width:65px;" id="inputClockTo" name="inputClockTo" placeholder="<?php echo _('HH:mm'); ?>">
		<div>
			Leave blank to not filter between time...
		</div>
	</div>


	<div class="form-group">
		<label for="inputTitle"><?php echo _('Days in week'); ?></label><br />
		<div class="checkbox">
			<label>
				<input type="checkbox" name="checkboxDay[]" id="checkboxDay1" value="1" checked /> <?php echo _('Monday'); ?>
			</label>
			<label>
				<input type="checkbox" name="checkboxDay[]" id="checkboxDay1" value="1" checked /> <?php echo _('Tuesday'); ?>
			</label>
			<label>
				<input type="checkbox" name="checkboxDay[]" id="checkboxDay1" value="1" checked /> <?php echo _('Wednesday'); ?>
			</label>
			<label>
				<input type="checkbox" name="checkboxDay[]" id="checkboxDay1" value="1" checked /> <?php echo _('Thursday'); ?>
			</label>
			<label>
				<input type="checkbox" name="checkboxDay[]" id="checkboxDay1" value="1" checked /> <?php echo _('Friday'); ?>
			</label>
			<label>
				<input type="checkbox" name="checkboxDay[]" id="checkboxDay1" value="1" checked /> <?php echo _('Saturday'); ?>
			</label>
			<label>
				<input type="checkbox" name="checkboxDay[]" id="checkboxDay1" value="1" checked /> <?php echo _('Sunday'); ?>
			</label>
		</div>
	</div>

	<div class="form-group">
		<label for="inputTitle"><?php echo _('Interval'); ?></label>
		<select class="form-control" name="selectIntervalHour" id="selectIntervalHour">
			<?php for ($i=0; $i < 23; $i++): ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			<?php endif; ?>
		</select>
		<select class="form-control" name="selectIntervalMin" id="selectIntervalMin">
			<?php for ($i=0; $i < 59; $i++): ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			<?php endif; ?>
		</select>

		<div>
			Interval is the (pause) time since last trigger. This is to prevent unnecessary server-load, and pulling from third-parties.
			It's also to prevent spamming you with messages (mail, sms, etc.) if turned on. Strongly suggest this is set to as high as acceptable for you.
		</div>
	</div>

	<button class="btn btn-primary" type="submit"><?php echo _('Create event'); ?></button>
</form>