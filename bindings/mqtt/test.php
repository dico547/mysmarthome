<?php
	/** PHP settings */
	date_default_timezone_set('Europe/Oslo');
	setlocale(LC_ALL, 'no_Nb'); 
	error_reporting(E_ALL ^ E_NOTICE);

	require_once( dirname(__FILE__) . '/phpMQTT.php' );
		
	$mqtt = new phpMQTT("iot.eclipse.org", 1883, "phpMQTT Sub Example"); //Change client name to something unique

	if(!$mqtt->connect()){
		exit(1);
	}

	$topics['hello/world'] = array(
		"qos"=>0, 
		"function"=>"procmsg"
	);

	$mqtt->subscribe($topics,0);

	$c = 0;
	while($mqtt->proc()){
		$c++;
		//if ($c == 100) break;
	}
	$mqtt->close();



	function procmsg($topic,$msg){
		//echo "Msg Recieved: ".date("r")."\nTopic:{$topic}\n$msg\n";
		echo "$msg\n";


	}
	
?>