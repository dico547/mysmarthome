<?php
	
	/* Get config and bindings
	--------------------------------------------------------------------------- */
	//include("config.inc.php");
	require_once 'HTTP/OAuth/Consumer.php';
	include("./msh-bindings/telldus.live/config.inc.php");
	include("./msh-bindings/telldus.live/binding.class.php");





	if (isset($_GET['debug'])) {
		$debug = true;
	} else {
		$debug = false; // <-- Set this for testing if GET is not set
	}



	/* Create obj and connect to telldus
	--------------------------------------------------------------------------- */
	$obj = new telldusLive($user['public_key'], $user['private_key'], $user['token'], $user['token_secret']);

	$path = "devices/list";
	$params = array('supportedMethods'=> 1023);

	$devices = $obj->telldusConnect($path, $params);


	if ($debug) {
		echo "<pre>";
			print_r($devices);
		echo "</pre>";
	}


	/* Loop devices and save/update in DB
	--------------------------------------------------------------------------- */
	foreach ($devices as $key => $data) {
		

		// Clear data
		unset($att);



		// Get data
		$att = $data->attributes();

		$device['id'] 			= trim($att->id);
		$device['name'] 		= trim($att->name);
		$device['state'] 		= trim($att->state);
		$device['statevalue'] 	= trim($att->statevalue);
		$device['methods'] 		= trim($att->methods);
		$device['type'] 		= trim($att->type);
		$device['clientName'] 	= trim($att->clientName);

		$descString = "state:{$device['state']};";
		$descString .= "statevalue:{$device['statevalue']};";
		$descString .= "methods:{$device['methods']};";
		$descString .= "clientName:{$device['clientName']};";

		if ($debug) {
			echo "<pre>";
				print_r($att);
			echo "</pre>";
		}


		// Set unit
		if ($device['type'] == "device") {
			$logUnit = "switch";
		} 

		elseif ($device['type'] == "group") {
			$logUnit = "group";
		}



		//$getIntID = getField("device_int_id", "fu_data_devices", "WHERE module LIKE 'futelldus' AND device_ext_id LIKE '{$device['id']}'");



		// Insert to DB
		if (empty($getIntID)) {
			$query = "INSERT INTO fu_data_devices SET 
						user_id='". $user['user_id'] ."', 
						module='futelldus', 
						device_ext_id='". $device['id'] ."', 
						device_name='". $device['name']  ."', 
						type='". $device['type'] ."', 
						description='". $descString ."',
						deactive='". 0 ."'";
			//$result = $mysqli->query($query);

			//$getIntID = $mysqli->insert_id;

			if ($debug) {
				echo "query: $query <br />";
				echo "Result: $result <br />";
			}
		}


		// Update DB
		else {
			$query = "UPDATE fu_data_devices SET 
						user_id='". $user['user_id'] ."', 
						module='futelldus', 
						device_ext_id='". $device['id'] ."', 
						device_name='". $device['name']  ."', 
						type='". $device['type'] ."', 
						description='". $descString ."', 
						deactive='". 0 ."'
						WHERE device_int_id LIKE '$getIntID'";
			//$result = $mysqli->query($query);

			if ($debug) {
				echo "query: $query <br />";
				echo "Result: $result <br />";
			}
		}



		$query = "INSERT INTO fu_data_log SET 
					device_int_id='". $getIntID ."', 
					user_id='". $user['user_id'] ."', 
					time='". time() ."', 
					value='". $device['state'] ."', 
					value2='". $device['statevalue'] ."', 
					ref='devices manually synced'";
		//$result = $mysqli->query($query);



		if ($debug) {
			echo "query: $query <br />";
			echo "Result: $result <br />";

			echo "<pre>";
				print_r($device);
			echo "</pre>";

			echo "<hr />";
		}


		unset($device);

	} //end-foreach


	if (!$debug) {
		//header("Location: ?m=futelldus&p=devices&msg=01");
		//exit();
	}

?>