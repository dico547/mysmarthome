<?php
	
	require_once( dirname(__FILE__) . '/../../core.php' );
	require_once 'HTTP/OAuth/Consumer.php';
	include_once(ABSPATH . "bindings/telldus.live/config.inc.php");
	include_once(ABSPATH . "bindings/telldus.live/binding.class.php");

	if (isset($_GET['debug'])) {
		$debug = true;
	} else {
		$debug = false;
	}



	$queryUsers = "SELECT * FROM msh_users WHERE deactive='0'";
	$resultUsers = $mysqli->query($queryUsers);
	$numRowsUsers = $resultUsers->num_rows;

	while ($rowUser = $resultUsers->fetch_array()) {

		$query = "SELECT * FROM msh_binding_tellduslive WHERE user_id='{$rowUser['user_id']}'";
		$result = $mysqli->query($query);
		$numRows = $result->num_rows;
		$telldusUser = $result->fetch_array();



		$obj = new telldusLive($telldusUser['public_key'], $telldusUser['private_key'], $telldusUser['token'], $telldusUser['token_secret']);

		
		// SYNC DEVICES
		$response = $obj->getDevices();


		$syncedDevices = array();
		$existingDevices = array();


		foreach ($response as $key => $data) {

			// Clear data
			unset($att);
			unset($device);
			unset($supportedMethods);
			unset($setMethod);
			$type = "";
			$category = "";
			$typeDesc = "";


			// Get data
			$att = $data->attributes();

			$device['id'] 			= trim($att->id);
			$device['name'] 		= trim($att->name);
			$device['state'] 		= trim($att->state);
			$device['statevalue'] 	= trim($att->statevalue);
			$device['methods'] 		= trim($att->methods);
			$device['type'] 		= trim($att->type);
			$device['clientName'] 	= trim($att->clientName);

			$descString = "state:{$device['state']};";
			$descString .= "statevalue:{$device['statevalue']};";
			$descString .= "methods:{$device['methods']};";
			$descString .= "clientName:{$device['clientName']};";


			// Does device exist?
			$query = "SELECT device_int_id FROM msh_devices WHERE binding LIKE 'telldus.live' AND device_ext_id LIKE '{$device['id']}'";
			$result = $mysqli->query($query);
			$thisDevice = $result->fetch_array();

			if (!empty($thisDevice['device_int_id'])) {
				$exist = true;
				$thisDevice = $objDevices->getDevice($thisDevice['device_int_id']);
			} else {
				$exist = false;
			}

			//echo "Device (telldus response): {$device['id']} - {$device['name']}<br />";
			//echo "Device exist? $exist<br />";
			//echo "This Device: {$thisDevice['device_int_id']}<br />";

			$supportedMethods = array();
			
			// Light ON/OFF
			if ($device['methods'] == 35) {
				$type = "device";
				$typeDesc = "onoff";
				$category = "light";

				if ($device['state'] == 1) $state = 1;
				elseif ($device['state'] == 2) $state = 0;

				// Supported methods
				$supportedMethods['turnOn'] = array('value'=>0);	//depricated
				$supportedMethods['turnOff'] = array('value'=>0);	//depricated

				$setMethod[] = array(
					'method_id' => 1,
					'value'=>0
				);

				$setMethod[] = array(
					'method_id' => 2,
					'value'=>0
				);

				$unitID = 4;
			}

			
			// Dim
			elseif ($device['methods'] == 51 && $device['type'] == "device") {
				$type = "device";
				$typeDesc = "dim";
				$category = "light";

				if ($device['state'] == 1) $state = 1;
				elseif ($device['state'] == 16) $state = 1;
				elseif ($device['state'] == 2) $state = 0;


				// Supported methods
				$supportedMethods['turnOn'] = array('value'=>0);	//depricated
				$supportedMethods['turnOff'] = array('value'=>0);	//depricated
				$supportedMethods['dim'] = array('value'=>1);		//depricated

				$setMethod[] = array(
					'method_id' => 1,
					'value'=>0
				);

				$setMethod[] = array(
					'method_id' => 2,
					'value'=>0
				);

				$setMethod[] = array(
					'method_id' => 3,
					'value'=>1
				);


				$unitID = 4;
			}
			
			// Groups
			elseif ($device['methods'] == 51 && $device['type'] == "group") {
				$type = "device";
				$typeDesc = "onoff";
				$category = "light";

				if ($device['state'] == 1) $state = 1;
				elseif ($device['state'] == 2) $state = 0;


				// Supported methods
				$supportedMethods['turnOn'] = array('value'=>0);	//depricated
				$supportedMethods['turnOff'] = array('value'=>0);	//depricated

				$setMethod[] = array(
					'method_id' => 1,
					'value'=>0
				);

				$setMethod[] = array(
					'method_id' => 2,
					'value'=>0
				);

				$unitID = 4;
			}


			// Bell
			elseif ($device['methods'] == 36 && $device['type'] == "device") {
				$type = "bell";
				$typeDesc = "";
				$category = "";

				//if ($device['state'] == 1) $state = 1;
				//elseif ($device['state'] == 2) $state = 0;

				// Supported methods
				$supportedMethods['bell'] = array('value'=>0);	//depricated

				$setMethod[] = array(
					'method_id' => 4,
					'value'=>0
				);

				$unitID = 15;
			}




			// Not exist => Insert to DB
			if (!$exist) {
				if ($debug) echo 'Inserting new device<br />';

				$icon = '<i class=\"fa fa-lightbulb-o\"></i>';

				$query = "INSERT INTO msh_devices SET 
							user_id='". $rowUser['user_id'] ."', 
							module='".$category."', 
							binding='telldus.live', 
							device_ext_id='". $device['id'] ."', 
							device_name='". $device['name']  ."', 
							type='". $type ."', 
							type_desc='". $typeDesc ."', 
							category='". $category ."', 
							description='". $descString ."',
							icon='". $icon ."',
							deactive='". 0 ."'";
				$result = $mysqli->query($query);

				$thisDevice['device_int_id'] = $mysqli->insert_id;


				// Add category to device
				$query = "INSERT INTO msh_devices_has_category SET 
							device_int_id='".$thisDevice['deviceIntID']."', 
							category_id='1'";
				$result = $mysqli->query($query);

				//echo "Insert result: $result <br />";
				//if (!$result) echo "Query: $query<br />";
			}


			// Exist => Update
			else {
				if ($debug) echo 'Updating existing device (ID: '.$thisDevice['deviceIntID'].')<br />';

				$query = "UPDATE msh_devices SET 
							user_id='". $rowUser['user_id'] ."', 
							module='".$category."', 
							binding='telldus.live', 
							device_ext_id='". $device['id'] ."', 
							device_name='". $device['name']  ."', 
							type='". $type ."', 
							type_desc='". $typeDesc ."', 
							category='". $category ."', 
							description='". $descString ."',
							deactive='". 0 ."'
							WHERE device_int_id='{$thisDevice['deviceIntID']}'";
				$result = $mysqli->query($query);

				//echo "Update result: $result <br />";
				//if (!$result) echo "Query: $query<br />";
			}


			// Insert supported methods
			$numMethods = count($supportedMethods);
			if ($numMethods > 0) {

				// DEPRICATED
				foreach ($supportedMethods as $method => $methodParams) {
					$query = "REPLACE INTO msh_devices_methods SET 
								device_int_id='". $thisDevice['deviceIntID'] ."', 
								method='". $method ."', 
								value='". $methodParams['value'] ."'";
					$result = $mysqli->query($query);
				}
			}

			$numMethods = count($setMethod);
			if ($numMethods > 0) {
				foreach ($setMethod as $key => $mData) {
					if (!empty($mData['method_id'])) {
						$query = "REPLACE INTO msh_devices_has_methods SET 
									d_id='". $thisDevice['deviceIntID'] ."', 
									m_id='". $mData['method_id'] ."'";
						$result = $mysqli->query($query);
					}
				}
			}


			// Insert values
			if ($thisDevice['monitor'] == 1) {
				$query = "INSERT INTO msh_data_log SET 
							device_int_id='". $thisDevice['deviceIntID'] ."', 
							user_id='". $rowUser['user_id'] ."', 
							time='". time() ."', 
							type='". 0  ."', 
							value='". $state ."', 
							ref='". $typeDesc ."'";
				$result = $mysqli->query($query);


				$p = array (
					'device_int_id' => $thisDevice['deviceIntID'],
					'time' => time(),
					'unit_id' => $unitID,
					'value' => $state,
				);

				$addLogResult = $objDevices->addLog($p);

				echo "<pre>";
					print_r($addLogResult);
				echo "</pre>";
				
			}



			// Put synces devices in array to compare to already synced devices.
			// This is used to deactivate remote deleted devices under here.
			$syncedDevices[$device['name']] = $device['id'];

			if ($debug) echo "<hr />";
		}



		// DEACTIVATE DEPRICATED DEVICES
		$query = "SELECT * 
				  FROM msh_devices 
				  WHERE binding LIKE 'telldus.live'
				  	AND type LIKE 'device'
				  	AND user_id='{$rowUser['user_id']}'
				  	AND deactive='0'";
		$result = $mysqli->query($query);
		$numRows = $result->num_rows;

		while ($row = $result->fetch_array()) {
			$existingDevices[$row['device_name']] = $row['device_ext_id'];
			//echo " - {$row['device_name']}<br />";
		}


		$diff = array_diff($existingDevices, $syncedDevices);

		foreach ($diff as $deviceName => $deviceExtID) {
			$query = "UPDATE msh_devices SET 
						monitor='". 0 ."', 
						public='". 0 ."', 
						deactive='". 1 ."' 
						WHERE (
								binding LIKE 'telldus.live' 
								AND device_ext_id LIKE '$deviceExtID'
								AND user_id='{$rowUser['user_id']}'
							  )";
			$result = $mysqli->query($query);
		}
		





		// SYNC DEVICES (SENSORS)
		$response = $obj->getSensors();

		$syncedSensors = array();
		$existingSensors = array();



		foreach ($response as $key => $data) {
			
			// Clear data
			unset($att);
			unset($device);
			unset($setMethod);
			$type = "";
			$category = "";
			$typeDesc = "";


			if ($debug) {
				echo "<pre>";
					print_r($data);
				echo "</pre>";
			}


			// Get data
			$att = $data->attributes();

			$device['id'] 			= trim($att->id);
			$device['name'] 		= trim($att->name);
			$device['lastUpdated'] 	= trim($att->lastUpdated);
			$device['ignored'] 		= trim($att->ignored);
			$device['client'] 		= trim($att->client);
			$device['clientName'] 	= trim($att->clientName);
			$device['online'] 		= trim($att->online);
			$device['editable'] 	= trim($att->editable);
			if (isset($att->temp)) 		$device['temp'] 		= trim($att->temp);
			if (isset($att->humidity)) 	$device['humidity'] 	= trim($att->humidity);


			if ($debug) {
				echo "<pre>";
					print_r($device);
				echo "</pre>";
			}


			// Does device exist?
			$query = "SELECT device_int_id FROM msh_devices WHERE binding LIKE 'telldus.live' AND device_ext_id LIKE '{$device['id']}'";
			$result = $mysqli->query($query);
			$thisDevice = $result->fetch_array();

			if (!empty($thisDevice['device_int_id'])) {
				$exist = true;
				$thisDevice = $objDevices->getDevice($thisDevice['device_int_id']);
			} else {
				$exist = false;
			}



			// Not exist => Insert to DB
			if (!$exist) {
				if ($debug) echo 'Inserting new device (sensor)<br />';
				$icon = '<i class=\"wi wi-thermometer\"></i>';

				$query = "INSERT INTO msh_devices SET 
							user_id='". $rowUser['user_id'] ."', 
							binding='telldus.live', 
							device_ext_id='". $device['id'] ."', 
							device_name='". $device['name']  ."', 
							description='". $descString ."',
							icon='". $icon ."',
							monitor='". 1 ."',
							deactive='". 0 ."'";
				$result = $mysqli->query($query);


				// Add category to device
				$query = "INSERT INTO msh_devices_has_category SET 
							device_int_id='".$thisDevice['deviceIntID']."', 
							category_id='2'";
				$result = $mysqli->query($query);

				//echo "Insert result: $result <br />";
				//if (!$result) echo "Query: $query<br />";
			}


			// Exist => Update
			else {
				if ($debug) echo 'Updateing existing device (sensor) (ID: '.$thisDevice['deviceIntID'].')<br />';

				$query = "UPDATE msh_devices SET 
							user_id='". $rowUser['user_id'] ."', 
							binding='telldus.live', 
							device_ext_id='". $device['id'] ."', 
							device_name='". $device['name']  ."', 
							description='". $descString ."',
							deactive='". 0 ."'
							WHERE device_int_id='{$thisDevice['deviceIntID']}'";
				$result = $mysqli->query($query);

				//echo "Update result: $result <br />";
				//if (!$result) echo "Query: $query<br />";
			}



			// Set method
			$setMethod[] = array(
				'method_id' => 5,
				'value'=>0
			);

			$numMethods = count($setMethod);
			if ($numMethods > 0) {
				foreach ($setMethod as $key => $mData) {
					if (!empty($mData['method_id'])) {
						$query = "REPLACE INTO msh_devices_has_methods SET 
									d_id='". $thisDevice['deviceIntID'] ."', 
									m_id='". $mData['method_id'] ."'";
						$result = $mysqli->query($query);
					}
				}
			}


			// DEPRICATED
			$query = "REPLACE INTO msh_devices_methods SET 
						device_int_id='". $thisDevice['deviceIntID'] ."', 
						method='value', 
						value='". 0 ."'";
			$result = $mysqli->query($query);

			



			// Insert values
			if ($device['lastUpdated'] != 0 && $thisDevice['monitor'] == 1) {
				if ($debug) echo 'Logging values<br />';

				$query = "INSERT INTO msh_data_log SET 
							device_int_id='". $thisDevice['deviceIntID'] ."', 
							user_id='". $rowUser['user_id'] ."', 
							time='". $device['lastUpdated'] ."', 
							type='". 0  ."', 
							value='". $device['temp'] ."', 
							value2='". $device['humidity'] ."', 
							ref='". $typeDesc ."'";
				$result = $mysqli->query($query);


				// New log: Temperature
				if (isset($device['temp'])) {
					unset($addLogResultTemp);

					$p = array (
						'device_int_id' => $thisDevice['deviceIntID'],
						'time' => $device['lastUpdated'],
						'unit_id' => 1,
						'value' => $device['temp'],
					);
					$addLogResultTemp = $objDevices->addLog($p);

					if ($debug) {
						echo "<b>Temperature log</b><br />\n";
						echo "<pre>";
							print_r($addLogResultTemp);
						echo "</pre>";
					}
				}
				elseif ($debug) {
					echo "<b>NOT logging Temp. Not set.</b><br />\n";
				}


				// New log: Humidity
				unset($addLogResultHum);

				if (isset($device['humidity'])) {
					$p = array (
						'device_int_id' => $thisDevice['deviceIntID'],
						'time' => $device['lastUpdated'],
						'unit_id' => 2,
						'value' => $device['humidity'],
					);
					$addLogResultHum = $objDevices->addLog($p);

					if ($debug) {
						echo "<b>Humidity log</b><br />\n";
						echo "<pre>";
							print_r($addLogResultHum);
						echo "</pre>";
					}
				}
				elseif ($debug) {
					echo "<b>NOT logging Humidity. Not set.</b><br />\n";
				}

				

			} 

			else {
				if ($debug) {
					echo 'NOT logging values for this sensor<br />';
					echo "lastUpdated: {$device['lastUpdated']} <br />";
					echo "monitor: {$thisDevice['monitor']} <br />";

					/*echo "<pre>";
						print_r($thisDevice);
					echo "</pre>";*/
					
				}
			}



			// Put synces devices in array to compare to already synced devices.
			// This is used to deactivate remote deleted devices under here.
			$syncedSensors[$device['name']] = $device['id'];

			if ($debug) echo "<hr />";
		}



		// DEACTIVATE DEPRICATED DEVICES
		$query = "SELECT * 
				  FROM msh_devices 
				  WHERE binding LIKE 'telldus.live'
				  	AND type LIKE 'sensor'
				  	AND user_id='{$rowUser['user_id']}'
				  	AND deactive='0'";
		$result = $mysqli->query($query);
		$numRows = $result->num_rows;

		while ($row = $result->fetch_array()) {
			$existingSensors[$row['device_name']] = $row['device_ext_id'];
			//echo " - {$row['device_name']}<br />";
		}


		$diff = array_diff($existingSensors, $syncedSensors);

		foreach ($diff as $deviceName => $deviceExtID) {
			$query = "UPDATE msh_devices SET 
						monitor='". 0 ."', 
						public='". 0 ."', 
						deactive='". 1 ."' 
						WHERE (
								binding LIKE 'telldus.live' 
								AND device_ext_id LIKE '$deviceExtID'
								AND user_id='{$rowUser['user_id']}'
							  )";
			$result = $mysqli->query($query);
		}

	}

?>