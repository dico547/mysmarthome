<h3>Telldus Binding</h3>



<h4 style="margin-top:45px;">Telldus keys</h4>
<?php
	

	/* Get userdata
	--------------------------------------------------------------------------- */
	$query = "SELECT * FROM msh_binding_tellduslive WHERE user_id='{$thisUser['user_id']}'";
	$result = $mysqli->query($query);
	$numRows = $result->num_rows;
	$telldusData = $result->fetch_array();



	// FORM
	echo "<form class='form-horizontal' role='form' action='".BINDINGS_URL."/telldus.live/execute.php?action=saveConfig' method='POST'>";
		

		// Public key
		echo "<div class='form-group'>";
			echo "<label for='inputTelldusPublicKey' class='col-sm-2 control-label'>"._('Public key')."</label>";
			echo "<div class='col-sm-10'>";
				echo "<input class='form-control' style='max-width:400px;' type='text' id='inputTelldusPublicKey' name='inputTelldusPublicKey' placeholder='"._('Insert public key')."' value='{$telldusData['public_key']}' />";
			echo "</div>";
		echo "</div>";



		// Private key
		echo "<div class='form-group'>";
			echo "<label for='inputTelldusPrivateKey' class='col-sm-2 control-label'>"._('Private key')."</label>";
			echo "<div class='col-sm-10'>";
				echo "<input class='form-control' style='max-width:400px;' type='text' id='inputTelldusPrivateKey' name='inputTelldusPrivateKey' placeholder='"._('Insert private')."' value='{$telldusData['private_key']}' />";
			echo "</div>";
		echo "</div>";


		// Token
		echo "<div class='form-group'>";
			echo "<label for='inputTelldusToken' class='col-sm-2 control-label'>"._('Token')."</label>";
			echo "<div class='col-sm-10'>";
				echo "<input class='form-control' style='max-width:400px;' type='text' id='inputTelldusToken' name='inputTelldusToken' placeholder='"._('Insert token')."' value='{$telldusData['token']}' />";
			echo "</div>";
		echo "</div>";


		// Token secret
		echo "<div class='form-group'>";
			echo "<label for='inputTelldusTokenSecret' class='col-sm-2 control-label'>"._('Token secret')."</label>";
			echo "<div class='col-sm-10'>";
				echo "<input class='form-control' style='max-width:400px;' type='text' id='inputTelldusTokenSecret' name='inputTelldusTokenSecret' placeholder='"._('Insert Token secret')."' value='{$telldusData['token_secret']}' />";
			echo "</div>";
		echo "</div>";
		

		echo "<div class='form-group'>";
			echo "<div class='col-sm-offset-2 col-sm-10'>";
				
				echo "<input class='btn btn-primary' type='submit' name='submit' value='"._('Save')."' />";

			echo "</div>";
		echo "</div>";

	echo "</form>";

?>



<h4 style="margin-top:45px;">Sync sensors and devices</h4>

<?php
	
	// Sync devices
	echo "<a class='btn btn-success btn-lg showTooltip' title='"._('Sync')."' href='".BINDINGS_URL."/telldus.live/execute.php?action=sync'>";
		echo "<span class='icon'><i class='fa fa-refresh fa-fw'></i></span>";

		echo "<span class='title'>";
			echo _('Sync');
		echo "</span>";
	echo "</a>";


?>