<?php
	
	/* Get config and bindings
	--------------------------------------------------------------------------- */
	//include("config.inc.php");
	require_once 'HTTP/OAuth/Consumer.php';
	include("./msh-bindings/telldus.live/config.inc.php");
	include("./msh-bindings/telldus.live/binding.class.php");




	if (isset($_GET['debug'])) {
		$debug = true;
	} else {
		$debug = false; // <-- Set this for testing if GET is not set
	}





	/* Create obj and connect to telldus
	--------------------------------------------------------------------------- */
	$obj = new telldusLive($user['public_key'], $user['private_key'], $user['token'], $user['token_secret']);

	$path = "sensors/list";
	$params = array();

	$sensors = $obj->telldusConnect($path, $params);




	$syncedSensors = array();
	$existingSensors = array();

	/* Loop sensors and save/update in DB
	--------------------------------------------------------------------------- */
	foreach ($sensors as $key => $data) {
		

		// Get data
		$att = $data->attributes();

		$device['id'] 				= trim($att->id);
		$device['name'] 			= trim($att->name);
		$device['lastUpdated'] 		= trim($att->lastUpdated);
		$device['ignored'] 			= trim($att->ignored);
		$device['client'] 			= trim($att->client);
		$device['clientName'] 		= trim($att->clientName);
		$device['online'] 			= trim($att->online);
		$device['editable'] 		= trim($att->editable);

		$descString = "lastUpdated:{$device['lastUpdated']};";
		$descString .= "ignored:{$device['ignored']};";
		$descString .= "client:{$device['client']};";
		$descString .= "clientName:{$device['clientName']};";
		$descString .= "online:{$device['online']};";
		$descString .= "editable:{$device['editable']};";

		if ($debug) {
			echo "<pre>";
				print_r($device);
			echo "</pre>";
		}
		


		// Hardcode data
		$logUnit = "celsius/humidity";
		$type = "sensor";
		$category = "thermometer";



		$getIntID = getField("device_int_id", "fu_data_devices", "WHERE (binding LIKE 'telldus.live' AND device_ext_id LIKE '{$device['id']}')");

		if ($debug) {
			echo "Checking if sensor ({$device['id']}) exist...<br />";
			if (!empty($getIntID)) echo "Sensor $getIntID exist!<br />";
			else echo "Sensor NOT exist<br />";
		}


		// Insert to DB
		if (empty($getIntID)) {
			
			$query = "INSERT INTO fu_data_devices SET 
						user_id='". $user['user_id'] ."', 
						module='telldus', 
						binding='telldus.live', 
						device_ext_id='". $device['id'] ."', 
						device_name='". $device['name']  ."', 
						type='". $type ."', 
						category='". $category ."', 
						description='". $descString ."', 
						deactive='". 0 ."'";
			$result = $mysqli->query($query);
			

			if ($debug == true) {
				echo "Insert sensor {$device['id']}<br />";
				//echo "Result: $result <br />";
			}
		}


		// Update DB
		else {
			$query = "UPDATE fu_data_devices SET 
						user_id='". $user['user_id'] ."', 
						module='telldus', 
						binding='telldus.live', 
						device_ext_id='". $device['id'] ."', 
						device_name='". $device['name']  ."', 
						type='". $type ."', 
						category='". $category ."', 
						description='". $descString ."', 
						deactive='". 0 ."'
						WHERE device_int_id LIKE '$getIntID'";
			$result = $mysqli->query($query);

			if ($debug) {
				echo "Update sensor {$device['id']}<br />";
				//echo "Result: $result <br />";
			}
		}


		if ($debug) echo "<hr />";
		


		$syncedSensors[$device['name']] = $device['id'];
	}






	/* Fetch existing sensors from database
	--------------------------------------------------------------------------- */
	$query = "SELECT * 
			  FROM fu_data_devices 
			  WHERE binding LIKE 'telldus.live'
			  	AND type LIKE 'sensor'
			  	AND user_id='{$user['user_id']}'
			  	AND deactive='0'";
	$result = $mysqli->query($query);
	$numRows = $result->num_rows;

	while ($row = $result->fetch_array()) {
		$existingSensors[$row['device_name']] = $row['device_ext_id'];
	}


	$diff = array_diff($existingSensors, $syncedSensors);






	/* Deactivate sensors that no longer exist in telldus.live
	--------------------------------------------------------------------------- */
	foreach ($diff as $deviceName => $deviceExtID) {
		$query = "UPDATE fu_data_devices SET 
					monitor='". 0 ."', 
					public='". 0 ."', 
					deactive='". 1 ."' 
					WHERE (
							binding LIKE 'telldus.live' 
							AND device_ext_id LIKE '$deviceExtID'
							AND user_id='{$user['user_id']}'
						  )";
		$result = $mysqli->query($query);

		echo "QUERY: $query <br />";
		echo "RESULT: $result <br />";
	}




	echo "<div class='alert alert-success'>" . _('Telldus sensors is now synced!') . "</div>";


	//header("Location: ?m=futelldus&p=sensors&msg=01");
	exit();

?>