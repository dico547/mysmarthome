<a class="pageBack" href="?m=settings">
	<i class="fa fa-chevron-left"></i> <?php echo _('Back'); ?>
</a>


<?php
	

	// Sync devices
	echo "<a class='button showTooltip' title='"._('Sync')."' href='msh-bindings/telldus.live/execute.php?action=sync'>";
		echo "<span class='icon'><i class='fa fa-refresh fa-fw'></i></span>";

		echo "<span class='title'>";
			echo _('Sync');
		echo "</span>";
	echo "</a>";


	/*
	// Sync devices
	echo "<a class='button showTooltip' title='"._('Sync devices')."' href='?m=settings&binding=telldus.live&p=settings_sync_devices&debug=true'>";
		echo "<span class='icon'><i class='fa fa-refresh fa-fw'></i></span>";

		echo "<span class='title'>";
			echo _('Sync devices');
		echo "</span>";
	echo "</a>";


	// Sync sensors
	echo "<a class='button showTooltip' title='"._('Sync sensors')."' href='?m=settings&binding=telldus.live&p=settings_sync_sensors&debug=true'>";
		echo "<span class='icon'><i class='fa fa-refresh fa-fw'></i></span>";

		echo "<span class='title'>";
			echo _('Sync sensors');
		echo "</span>";
	echo "</a>";
	*/


	// API Keys
	echo "<a class='button showTooltip' title='"._('API keys')."' href='?m=settings&binding=telldus.live&p=settings_keys'>";
		echo "<span class='icon'><i class='fa fa-key fa-fw'></i></span>";

		echo "<span class='title'>";
			echo _('API keys');
		echo "</span>";
	echo "</a>";


?>