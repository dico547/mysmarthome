<?php
	
	/* Get config and bindings
	--------------------------------------------------------------------------- */	
	require_once( dirname(__FILE__) . '/../../core.php' );
	require_once 'HTTP/OAuth/Consumer.php';
	include("./config.inc.php");
	include("./binding.class.php");




	$r = array();
	$error = false;
	



	/* Get parameters
	--------------------------------------------------------------------------- */
	if (isset($_GET['action'])) $action = clean($_GET['action']);
	if (isset($_GET['deviceID'])) $deviceID = clean($_GET['deviceID']);

	if (isset($_GET['ajax']) || isset($_GET['json'])) {
		$json = true;
	}

	if (isset($_GET['output'])) {
		$output = true;
	}


	$r['GET'] = $_GET;



	/* Error check
	--------------------------------------------------------------------------- */

	// If thisUser auth parameters are missing
	if (!isset($thisUser['user_id']) || empty($thisUser['user_id'])) {

		// Check if API code is provided
		if (isset($_GET['apikey'])) {
			$getUser = $objUsers->getUser($_GET['apikey']);
			$userID = $getUser['user_id'];
		}
	}

	// Set userID to login user if exist
	else {
		$userID = $thisUser['user_id'];
	}



	


	if (empty($action)) {
		$error = true;
		$r['error'][] = 'Action not provided';
	}


	if (empty($deviceID)) {
		$error = true;
		$r['error'][] = 'DeviceID not provided';
	}

	if (empty($userID)) {
		$error = true;
		$r['error'][] = 'UserID not provided';
	}





	/* Telldus parameters from DB
	--------------------------------------------------------------------------- */
	$result = $mysqli->query("SELECT * FROM msh_binding_tellduslive WHERE user_id='$userID'");
	$telldusUser = $result->fetch_array();


	//echo "{$thisUser['user_id']} - {$telldusUser['public_key']}, {$telldusUser['private_key']}, {$telldusUser['token']}, {$telldusUser['token_secret']}<br />";


	$obj = new telldusLive($telldusUser['public_key'], $telldusUser['private_key'], $telldusUser['token'], $telldusUser['token_secret']);
	$r['telldus_obj'] = $obj;

	$getDeviceIntID = $objDevices->getInternalID($deviceID);
	$r['device_int_id'] = $getDeviceIntID;

	//$getDeviceIntID = getField("device_int_id", "fu_data_devices", "WHERE (device_ext_id='$deviceID' AND binding LIKE 'telldus.live')");





	/* Turn ON
	--------------------------------------------------------------------------- */
	if ($action == 'turnOn' && !$error) {
		$result = $obj->turnOn($deviceID);
		
		// Log action
		$p = array (
			'device_int_id' => $getDeviceIntID,
			'time' => time(),
			'unit_id' => 4,
			'value' => 1,
		);
		$addLogResult = $objDevices->addLog($p);

		// Set return data
		$r['action']['result'] = $result;
		$r['action']['log'] = $addLogResult;


		if ($json) {
			echo json_encode($r);
		}
		elseif($output) {
			echo "<pre>";
				print_r($r);
			echo "</pre>";
		}
		else {
			header('Location: ' . $_SERVER['HTTP_REFERER']);			
		}
			
		exit();
	}




	/* Turn OFF
	--------------------------------------------------------------------------- */
	if ($action == "turnOff") {
		$result = $obj->turnOff($deviceID);

		// Log action
		$p = array (
			'device_int_id' => $getDeviceIntID,
			'time' => time(),
			'unit_id' => 4,
			'value' => 0,
		);
		$addLogResult = $objDevices->addLog($p);

		// Set return data
		$r['action']['result'] = $result;
		$r['action']['log'] = $addLogResult;

		if ($json) {
			echo json_encode($r);
		}
		elseif($output) {
			echo "<pre>";
				print_r($r);
			echo "</pre>";
		}
		else {
			header('Location: ' . $_SERVER['HTTP_REFERER']);				
		}
			
		exit();
	}




	/* Dim light
	--------------------------------------------------------------------------- */
	if ($action == "dim") {
		$dimValue = $_GET['dimvalue'];
		$result = $obj->dim($deviceID, $dimValue);

		
		if ($result['status'] == 'success') {
			$p = array (
				'device_int_id' => $getDeviceIntID,
				'time' => time(),
				'unit_id' => 4,
				'value' => 1,
				'state' => $dimValue,
			);
			$addLogResult = $objDevices->addLog($p);
		}

		// Set return data
		$r['action']['result'] = $result;
		$r['action']['log'] = $addLogResult;


		if ($json) {
			echo json_encode($r);
		}
		elseif($output) {
			echo "<pre>";
				print_r($r);
			echo "</pre>";
		}
		else {
			header('Location: ' . $_SERVER['HTTP_REFERER']);			
		}
			
		exit();
	}





	/* Turn ON
	--------------------------------------------------------------------------- */
	if ($action == "bell") {		
		$result = $obj->bell($deviceID);


		// Set return data
		$r['action']['result'] = $result;
		$r['action']['log'] = $addLogResult;
		

		if ($json) {
			echo json_encode($r);
		}
		elseif($output) {
			echo "<pre>";
				print_r($r);
			echo "</pre>";
		}
		else {
			header('Location: ' . $_SERVER['HTTP_REFERER']);				
		}
			
		exit();
	}







	/* Save config
	--------------------------------------------------------------------------- */
	if ($action == "saveConfig") {

		$input['public_key'] 	= clean($_POST['inputTelldusPublicKey']);
		$input['private_key'] 	= clean($_POST['inputTelldusPrivateKey']);
		$input['token'] 		= clean($_POST['inputTelldusToken']);
		$input['token_secret'] 	= clean($_POST['inputTelldusTokenSecret']);

		$query = "REPLACE INTO msh_binding_tellduslive SET 
					user_id='".$thisUser['user_id']."', 
					public_key='".$input['public_key']."', 
					private_key='".$input['private_key']."', 
					token='".$input['token']."', 
					token_secret='".$input['token_secret']."'";
		$result = $mysqli->query($query);

		// Set return data
		$r['action']['result'] = $result;


		if ($json) {
			echo json_encode($r);
		}
		elseif($output) {
			echo "<pre>";
				print_r($r);
			echo "</pre>";
		}
		else {
			header('Location: ' . $_SERVER['HTTP_REFERER']);				
		}
			
		exit();
	}





	/* Sync
	--------------------------------------------------------------------------- */
	if ($action == "sync") {
		include_once(ABSPATH . 'bindings/telldus.live/sync.php');

		if (!$json && !$output) {
			header('Location: ' . $_SERVER['HTTP_REFERER']);	
		}
			
		exit();
	}

?>