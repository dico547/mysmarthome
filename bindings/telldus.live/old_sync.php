<?php
	
	date_default_timezone_set('Europe/Oslo'); 
	setlocale(LC_ALL, 'no_Nb');

	
	/* Get config and bindings
	--------------------------------------------------------------------------- */
	include("/wwwroot/www/mysmarthome.no/mysmarthome/db.connect.inc.php");
	require_once 'HTTP/OAuth/Consumer.php';

	include("/wwwroot/www/mysmarthome.no/mysmarthome/msh-bindings/telldus.live/config.inc.php");
	include("/wwwroot/www/mysmarthome.no/mysmarthome/msh-bindings/telldus.live/binding.class.php");




	$debug = false;	// Print status messages


	echo "Rundate: " . date('d-m-Y H:i:s') . "\r\n";










	/* Loop all users
	--------------------------------------------------------------------------- */
	$resultUsers = $mysqli->query("SELECT * FROM fu_users WHERE deactive='0'");
	$numUsers = $resultUsers->num_rows;

	while ($rowUsers = $resultUsers->fetch_array()) {


		if ($debug) {
			echo "user_id: {$rowUsers['user_id']}<br />";
			echo "User: {$rowUsers['mail']}<br />";
		}



		// Telldus userdata
		$resultConfig = $mysqli->query("SELECT * FROM fu_binding_tellduslive WHERE user_id='{$rowUsers['user_id']}'");
		$telldusUser = $resultConfig->fetch_array();


		if ($debug) {
			echo "user_id: {$rowUsers['user_id']}<br />";
			echo "User: {$rowUsers['mail']}<br />";

			echo "public_key: {$telldusUser['public_key']}<br />";
			echo "private_key: {$telldusUser['private_key']}<br />";
			echo "token: {$telldusUser['token']}<br />";
			echo "token_secret: {$telldusUser['token_secret']}<br />";
		}
		

		// Create object with users telldus keys
		$obj = new telldusLive($telldusUser['public_key'], $telldusUser['private_key'], $telldusUser['token'], $telldusUser['token_secret']);











		/* Sync Telldus devices
		--------------------------------------------------------------------------- */
		$response = $obj->getDevices();


		$syncedDevices = array();
		$existingDevices = array();


		foreach ($response as $key => $data) {

			// Clear data
			unset($att);
			$type = "";
			$category = "";
			$typeDesc = "";


			// Get data
			$att = $data->attributes();

			$device['id'] 			= trim($att->id);
			$device['name'] 		= trim($att->name);
			$device['state'] 		= trim($att->state);
			$device['statevalue'] 	= trim($att->statevalue);
			$device['methods'] 		= trim($att->methods);
			$device['type'] 		= trim($att->type);
			$device['clientName'] 	= trim($att->clientName);

			$descString = "state:{$device['state']};";
			$descString .= "statevalue:{$device['statevalue']};";
			$descString .= "methods:{$device['methods']};";
			$descString .= "clientName:{$device['clientName']};";


			// Does device exist?
			$query = "SELECT device_int_id FROM fu_data_devices WHERE binding LIKE 'telldus.live' AND device_ext_id LIKE '{$device['id']}'";
			$result = $mysqli->query($query);
			$exist = $result->num_rows;
			$thisDevice = $result->fetch_array();

			//echo "Device (telldus response): {$device['id']} - {$device['name']}<br />";
			//echo "Device exist? $exist<br />";
			//echo "This Device: {$thisDevice['device_int_id']}<br />";
			

			if ($device['methods'] == 35) {
				$type = "device";
				$typeDesc = "onoff";
				$category = "light";
			}
			
			elseif ($device['methods'] == 51 && $device['type'] == "device") {
				$type = "device";
				$typeDesc = "dim";
				$category = "light";
			}
			
			elseif ($device['methods'] == 51 && $device['type'] == "group") {
				$type = "device";
				$typeDesc = "onoff";
				$category = "light";
			}


			// Not exist => Insert to DB
			if ($exist == 0) {
				//echo "Insert<br />";
				$query = "INSERT INTO fu_data_devices SET 
							user_id='". $rowUsers['user_id'] ."', 
							module='".$category."', 
							binding='telldus.live', 
							device_ext_id='". $device['id'] ."', 
							device_name='". $device['name']  ."', 
							type='". $device['type'] ."', 
							type_desc='". $typeDesc ."', 
							description='". $descString ."',
							deactive='". 0 ."'";
				$result = $mysqli->query($query);

				//echo "Insert result: $result <br />";
				if (!$result) echo "Query: $query<br />";
			}


			// Exist => Update
			else {
				//echo "Update<br />";
				$query = "UPDATE fu_data_devices SET 
							user_id='". $rowUsers['user_id'] ."', 
							module='".$category."', 
							binding='telldus.live', 
							device_ext_id='". $device['id'] ."', 
							device_name='". $device['name']  ."', 
							type='". $device['type'] ."', 
							type_desc='". $typeDesc ."', 
							description='". $descString ."',
							deactive='". 0 ."'
							WHERE device_int_id='{$thisDevice['device_int_id']}'";
				$result = $mysqli->query($query);

				//echo "Update result: $result <br />";
				if (!$result) echo "Query: $query<br />";
			}



			// Insert values
			$query = "INSERT INTO fu_data_log SET 
						device_int_id='". $thisDevice['device_int_id'] ."', 
						user_id='". $rowUsers['user_id'] ."', 
						time='". time() ."', 
						type='". 0  ."', 
						value='". $device['state'] ."', 
						ref='". $typeDesc ."'";
			$result = $mysqli->query($query);



			// Put synces devices in array to compare to already synced devices.
			// This is used to deactivate remote deleted devices under here.
			$syncedDevices[$device['name']] = $device['id'];

			//echo "<hr />";
		}



		// DEACTIVATE DEPRICATED DEVICES
		$query = "SELECT * 
				  FROM fu_data_devices 
				  WHERE binding LIKE 'telldus.live'
				  	AND type LIKE 'device'
				  	AND user_id='{$rowUsers['user_id']}'
				  	AND deactive='0'";
		$result = $mysqli->query($query);
		$numRows = $result->num_rows;

		while ($row = $result->fetch_array()) {
			$existingDevices[$row['device_name']] = $row['device_ext_id'];
			//echo " - {$row['device_name']}<br />";
		}


		$diff = array_diff($existingDevices, $syncedDevices);

		foreach ($diff as $deviceName => $deviceExtID) {
			$query = "UPDATE fu_data_devices SET 
						monitor='". 0 ."', 
						public='". 0 ."', 
						deactive='". 1 ."' 
						WHERE (
								binding LIKE 'telldus.live' 
								AND device_ext_id LIKE '$deviceExtID'
								AND user_id='{$rowUsers['user_id']}'
							  )";
			$result = $mysqli->query($query);
		}
		










		/* Sync Telldus sensors
		--------------------------------------------------------------------------- */
		$response = $obj->getSensors();

		/*
		echo "<pre>";
			print_r($response);
		echo "</pre>";
		*/

		$syncedSensors = array();
		$existingSensors = array();


		foreach ($response as $key => $data) {
			
			// Clear data
			unset($att);
			$type = "";
			$category = "";
			$typeDesc = "";
			$device['type'] = "sensor";


			// Get data
			$att = $data->attributes();

			$device['id'] 			= trim($att->id);
			$device['name'] 		= trim($att->name);
			$device['lastUpdated'] 	= trim($att->lastUpdated);
			$device['ignored'] 		= trim($att->ignored);
			$device['client'] 		= trim($att->client);
			$device['clientName'] 	= trim($att->clientName);
			$device['online'] 		= trim($att->online);
			$device['editable'] 	= trim($att->editable);
			$device['temp'] 		= trim($att->temp);
			$device['humidity'] 	= trim($att->humidity);


			// Does device exist?
			$query = "SELECT device_int_id FROM fu_data_devices WHERE binding LIKE 'telldus.live' AND device_ext_id LIKE '{$device['id']}'";
			$result = $mysqli->query($query);
			$exist = $result->num_rows;
			$thisDevice = $result->fetch_array();

			//echo "Sensor (telldus response): {$device['id']} - {$device['name']}<br />";
			//echo "Sensor exist? $exist<br />";
			//echo "This Sensor: {$thisDevice['device_int_id']}<br />";
			
			$type = "sensor";
			$category = "clima";

			if (!empty($device['temp']) && !empty($device['humidity'])) {
				$typeDesc = "temp/humidity";
			}

			elseif (!empty($device['temp'])) {
				$typeDesc = "temp";
			}

			elseif (!empty($device['humidity'])) {
				$typeDesc = "humidity";
			}



			// Not exist => Insert to DB
			if ($exist == 0) {
				//echo "Insert<br />";
				$query = "INSERT INTO fu_data_devices SET 
							user_id='". $rowUsers['user_id'] ."', 
							module='".$category."', 
							binding='telldus.live', 
							device_ext_id='". $device['id'] ."', 
							device_name='". $device['name']  ."', 
							type='". $device['type'] ."', 
							type_desc='". $typeDesc ."', 
							description='". $descString ."',
							deactive='". 0 ."'";
				$result = $mysqli->query($query);

				//echo "Insert result: $result <br />";
				//if (!$result) echo "Query: $query<br />";
			}


			// Exist => Update
			else {
				//echo "Update<br />";
				$query = "UPDATE fu_data_devices SET 
							user_id='". $rowUsers['user_id'] ."', 
							module='".$category."', 
							binding='telldus.live', 
							device_ext_id='". $device['id'] ."', 
							device_name='". $device['name']  ."', 
							type='". $device['type'] ."', 
							type_desc='". $typeDesc ."', 
							description='". $descString ."',
							deactive='". 0 ."'
							WHERE device_int_id='{$thisDevice['device_int_id']}'";
				$result = $mysqli->query($query);

				//echo "Update result: $result <br />";
				//if (!$result) echo "Query: $query<br />";
			}



			// Insert values
			if ($device['lastUpdated'] != 0) {
				$query = "INSERT INTO fu_data_log SET 
							device_int_id='". $thisDevice['device_int_id'] ."', 
							user_id='". $rowUsers['user_id'] ."', 
							time='". $device['lastUpdated'] ."', 
							type='". 0  ."', 
							value='". $device['temp'] ."', 
							value2='". $device['humidity'] ."', 
							ref='". $typeDesc ."'";
				$result = $mysqli->query($query);
			}



			// Put synces devices in array to compare to already synced devices.
			// This is used to deactivate remote deleted devices under here.
			$syncedSensors[$device['name']] = $device['id'];

			//echo "<hr />";
		}



		// DEACTIVATE DEPRICATED DEVICES
		$query = "SELECT * 
				  FROM fu_data_devices 
				  WHERE binding LIKE 'telldus.live'
				  	AND type LIKE 'sensor'
				  	AND user_id='{$rowUsers['user_id']}'
				  	AND deactive='0'";
		$result = $mysqli->query($query);
		$numRows = $result->num_rows;

		while ($row = $result->fetch_array()) {
			$existingSensors[$row['device_name']] = $row['device_ext_id'];
			//echo " - {$row['device_name']}<br />";
		}


		$diff = array_diff($existingSensors, $syncedSensors);

		foreach ($diff as $deviceName => $deviceExtID) {
			$query = "UPDATE fu_data_devices SET 
						monitor='". 0 ."', 
						public='". 0 ."', 
						deactive='". 1 ."' 
						WHERE (
								binding LIKE 'telldus.live' 
								AND device_ext_id LIKE '$deviceExtID'
								AND user_id='{$rowUsers['user_id']}'
							  )";
			$result = $mysqli->query($query);
		}






		if ($debug) echo "<hr />";

	} // end loop users





	


	/*
	if (!$ajax) {
		header("Location: ".$_SERVER['HTTP_REFERER']."&msgID=01&msg=successful");
	}
	*/

	exit();

?>