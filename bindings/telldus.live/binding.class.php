<?php

	class telldusLive
	{

		private $public_key;
		private $private_key;
		private $accessToken;
		private $accessTokenSecret;

		public function __construct($public_key, $private_key, $accessToken, $accessTokenSecret){
			$this->public_key = $public_key;
			$this->private_key = $private_key;
			$this->accessToken = $accessToken;
			$this->accessTokenSecret = $accessTokenSecret;
		}
		





		/**
		* Get all types of data with Telldus API path (TEST)
		*
		* @param  	String		$path			Path from Telldus explorer
		* @param  	Array		$params			Array with parameter values
		* @return  	Array		$xmlData		Array with data
		*/

		private function telldusConnect($path, $params) {
			$consumer = new HTTP_OAuth_Consumer($this->public_key, $this->private_key, $this->accessToken, $this->accessTokenSecret);

			//$params = array();
			$response = $consumer->sendRequest(constant('REQUEST_URI').'/'.$path, $params, 'GET');

			$xmlString = $response->getBody();
			$xmlData = new SimpleXMLElement($xmlString);

			return $xmlData;
		}













		/*
			BELOW HERE ARE STANDARD CLASS/FUNCTIONS THAT WILL SHOULD WORK ON CROSS-PLATFORM/HARDWARE.
							!!! All functions need to be standarized !!!

					Functions that are telldus live spesific must be placed below this block of 
					default mysmarthome functions!
			

			Standard functions:

				turnOn		- turns device on
				turnOff		- turns device off
				dim 		- dim the device
							  Dim must be between 0 (off) and 100 (full), if your hardware have other type of values, you have to convert them with your math skills!

		*/






		/**
		* Turn device on
		*
		* @param  	Int			$deviceID				Telldus Device ID
		* @return  	Array		$xmlData			Array with response
		*/

		public function turnOn($deviceID) {
			/*
			$consumer = new HTTP_OAuth_Consumer($this->public_key, $this->private_key, $this->accessToken, $this->accessTokenSecret);
			

			$params = array('id'=> $deviceID);
			$response = $consumer->sendRequest(constant('REQUEST_URI').'/device/turnOn', $params, 'GET');

			$xmlString = $response->getBody();
			$xmlData = new SimpleXMLElement($xmlString);
			*/

			$params = array('id'=> $deviceID);
			$response = $this->telldusConnect('device/turnOn', $params);

			return "Response: " . $response;
		}



		/**
		* Turn device off
		*
		* @param  	Int			$deviceID			Telldus Device ID
		* @return  	Array		$xmlData			Array with response
		*/

		public function turnOff($deviceID) {
			/*
			$consumer = new HTTP_OAuth_Consumer($this->public_key, $this->private_key, $this->accessToken, $this->accessTokenSecret);
			

			$params = array('id'=> $deviceID);
			$response = $consumer->sendRequest(constant('REQUEST_URI').'/device/turnOff', $params, 'GET');

			$xmlString = $response->getBody();
			$xmlData = new SimpleXMLElement($xmlString);
			*/

			$params = array('id'=> $deviceID);
			$this->telldusConnect('device/turnOff', $params);

			//return $xmlData;
		}



		/**
		* Dim the device
		*
		* @param  	Int			$deviceID			Telldus Device ID
		* @param  	Int			$dimValue			Dim value (0-100)
		* @return  	Array		$xmlData			Array with response
		*/

		public function dim($deviceID, $dimValue) {
			$r = array();
			
			// Convert to telldus values in range 0-255
			$telldusDimValue = ($dimValue * 2.55);
			$telldusDimValue = str_replace(',', '.', $telldusDimValue);

			// Dim light
			$params = array('id'=> $deviceID, 'level'=> $telldusDimValue);
			$response = $this->telldusConnect('device/dim', $params);

			// Trim feedback status
			$status = trim($response->status);

			// Return status parameters
			if ($status == 'success') {
				$r['status'] = 'success';
				$r['dim_value'] = $telldusDimValue;
			} else {
				$r['status'] = 'error';
			}

			return $r;
		}





		/**
		* Bell
		* Make device chime
		*
		* @param  	Int			$deviceID			Telldus Device ID
		* @return  	Array		$xmlData			Array with response
		*/

		public function bell($deviceID) {
			$params = array('id'=> $deviceID);
			$this->telldusConnect('device/bell', $params);
		}







		/* ----------------------------------- END MYSMARTHOME DEFAULT PARAMS ----------------------------------- */























		/**
		* Get device list
		*
		* @return  	Array		$xmlData			Array with all the devices
		*/

		public function getDevices() {

			$params = array('supportedMethods'=> 1023);
			$response = $this->telldusConnect("devices/list", $params);

			return $response;
		}



		/**
		* Get sensor list
		*
		* @return  	Array		$xmlData			Array with all the sensors
		*/

		public function getSensors() {

			$params = array('includeValues'=> 1);
			$response = $this->telldusConnect("sensors/list", $params);

			return $response;
		}




		/**
		* Get sensor single values
		*
		* @return  	Array		$xmlData			Array with all the sensors
		*/

		public function getSensorsValues($sensorID) {
			$consumer = new HTTP_OAuth_Consumer($this->public_key, $this->private_key, $this->accessToken, $this->accessTokenSecret);
			

			$params = array('id'=> $sensorID);
			$response = $consumer->sendRequest(constant('REQUEST_URI').'/sensor/info', $params, 'GET');

			$xmlString = $response->getBody();
			$xmlData = new SimpleXMLElement($xmlString);

			return $xmlData;
		}

	}


?>