--
-- Tabellstruktur for tabell `msh_binding_tellduslive`
--

CREATE TABLE IF NOT EXISTS `msh_binding_tellduslive` (
  `user_id` int(11) NOT NULL,
  `public_key` varchar(256) NOT NULL,
  `private_key` varchar(256) NOT NULL,
  `token` varchar(256) NOT NULL,
  `token_secret` varchar(256) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


--
-- Dataark for tabell `msh_cronjobs`
--

INSERT INTO `msh_cronjobs` (`title`, `filepath`, `interval`, `last_run`, `disabled`) VALUES
('Telldus sync', 'bindings/telldus.live/sync.php', 900, 0, 0);