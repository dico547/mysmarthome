<?php
	require_once( dirname(__FILE__) . '/../../core.php' );




	$filePath = 'upload/nmap.xml'; // Relative path (NOT IN USE??)
	$filePathAbs = ABSPATH . 'bindings/netscanner/' . $filePath; // Absolute path
	
	//$filePath = '/wwwroot/www/mysmarthome.no/beta/app/upload/nmap.xml';
	
	$debug = true;
	$deleteFile = false; // Delete XML file after sync

	/*$logDevices = array(
		'192.168.1.127' => 'Robert iPhone', 
		'192.168.1.141' => 'Veronica iPhone'
	);*/

	$logDevices = array(
		'192.168.1.127', 
		'192.168.1.141',
	);




	if ($debug) {
		echo "filePath: $filePath<br />";
		echo "filePathAbs: $filePathAbs<br />";
	}



	if (file_exists($filePathAbs)) {
		if ($debug) echo "File exists. Processing... <br />";

		//$devicesObj = new devices;
		$syncedDevices = array();




		$result = simplexml_load_file($filePathAbs); // Parse XML

		// Fetch time nmap complete
		$syncTime = clean($result->runstats->finished['time']);


		if ($debug) {
			echo "Time synced: " . date('d-m-Y H:i:s', $syncTime) . "<br />";
			echo "<hr />";
		}
		
		/*echo "<pre>";
			print_r($result->host);
		echo "</pre>";
		*/

		// If writing to DB fails, variable will be set to false and file will not be deleted
		$syncComplete = true;

		// Loop hosts
		foreach ($result->host as $key => $hostData) {

			/*echo "<pre>";
				print_r($hostData);
			echo "</pre>";*/
			

			// Clean date
			$ip_adress = clean($hostData->address[0]['addr']);
			if (in_array($ip_adress, $logDevices)) {
				if ($debug) echo "Device found: $ip_adress. Processing... <br />";

				$mac_adress = clean($hostData->address[1]['addr']);
				$hostname = clean($hostData->address[1]['vendor']);

				if (!empty($mac_adress)) {
					if ($debug) echo "MAC found: $mac_adress. Processing... <br />";
					$syncedDevices[] = $mac_adress;


					if ($debug) {
						echo "IP: {$hostData->address[0]['addr']}<br />";
						echo "MAC: {$hostData->address[1]['addr']}<br />";
						echo "Hostname: {$hostData->address[1]['vendor']}<br />";
						echo "Last_seen: $syncTime - (".date('d-m-Y H:i', $syncTime).")<br />";
					}



					// Check if exist
					$query = "SELECT * FROM msh_devices WHERE device_ext_id LIKE '$mac_adress'";
					$result = $mysqli->query($query);
					$numRows = $result->num_rows;


					// Update if exist
					if ($numRows == 1) {
						if ($debug) echo "Device found. Updateing... <br />";
						$thisDevice = $result->fetch_array();

						
						// Update MSH-device
						$params = array (
											"device_int_id" => $thisDevice['device_int_id'],
											"user_id" => 1,
											"module" => "network",
											"binding" => "netscanner",
											"device_name" => $ip_adress,
											"type" => "device",
											"type_desc" => "networkdevice",
											"category" => "network",
											"description" => $hostname,
											"state" => 1,
										);

						
						$result = $objDevices->updateDevice($params);

						if ($result) {
							if ($debug) echo "Class: Updated ($result)<br />";
						}
						else {
							$syncComplete = false;
							if ($debug) echo "ERROR: Class: Updated ($result)<br />";
						}


						

						if (in_array($ip_adress, $logDevices)) {
							echo "Exist: Log device {$thisDevice['device_int_id']} <br />";
							$p = array (
								'device_int_id' => $thisDevice['device_int_id'],
								'time' => $syncTime,
								'unit_id' => 4,
								'value' => 1,
							);

							$addLogResult = $objDevices->addLog($p);

							echo "<pre>";
								print_r($addLogResult);
							echo "</pre>";


							// Set method
							$setMethod[] = array(
								'method_id' => 5,
								'value'=>0
							);
							$numMethods = count($setMethod);
							if ($numMethods > 0) {
								foreach ($setMethod as $key => $mData) {
									if (!empty($mData['method_id'])) {
										$query = "REPLACE INTO msh_devices_has_methods SET 
													d_id='". $thisDevice['device_int_id'] ."', 
													m_id='". $mData['method_id'] ."'";
										$result = $mysqli->query($query);
									}
								}
							}


						} else {
							echo "Not exist, Not logging device {$thisDevice['device_int_id']} <br />";
						}
					}






					// Insert if not exist
					else {

						// Insert new MSH-device
						$icon = '<i class=\"fa fa-globe\"></i>';

						$params = array (
											"user_id" => 1,
											"module" => "network",
											"binding" => "netscanner",
											"device_ext_id" => $mac_adress,
											"device_name" => $ip_adress,
											"device_alias" => $logDevices[$ip_adress],
											"type" => "device",
											"type_desc" => "networkdevice",
											"category" => "network",
											"description" => $hostname, 
											"icon" => $icon,
											"state" => 1,
										);

						if (!empty($mac_adress)) {
							$deviceIntID = $objDevices->addDevice($params);
						}

						if ($result) {
							if ($debug) echo "Class: Inserted/added ($result)<br />";

							// Add category to device
							$query = "INSERT INTO msh_devices_has_category SET 
										device_int_id='".$deviceIntID."', 
										category_id='10'";
							$result = $mysqli->query($query);
						}
						else {
							$syncComplete = false;
							if ($debug) echo "ERROR: Class: Inserted/added ($result)<br />";
						}



						// Add log
						if (in_array($ip_adress, $logDevices)) {
							echo "New: Log device {$thisDevice['device_int_id']} <br />";
							$p = array (
								'device_int_id' => $deviceIntID,
								'time' => $syncTime,
								'unit_id' => 4,
								'value' => 1,
							);

							$addLogResult = $objDevices->addLog($p);

							echo "<pre>";
								print_r($addLogResult);
							echo "</pre>";

							// Set method
							$setMethod[] = array(
								'method_id' => 5,
								'value'=>0
							);
							$numMethods = count($setMethod);
							if ($numMethods > 0) {
								foreach ($setMethod as $key => $mData) {
									if (!empty($mData['method_id'])) {
										$query = "REPLACE INTO msh_devices_has_methods SET 
													d_id='". $deviceIntID ."', 
													m_id='". $mData['method_id'] ."'";
										$result = $mysqli->query($query);
									}
								}
							}

							
						} else {
							echo "Not exist, Not logging device $deviceIntID <br />";
						}


						
					}
					

					if ($debug) echo "<hr />";
				} //end-check-mac-not-empty
			} //end-check-if-log-this-ip

			else {
				if ($debug) echo "ERROR Device NOT exist in whitelist: $ip_adress. <br />";
			}
		} //end-foreach


		// Delete file when sync if complete
		if ($syncComplete) {
			if ($debug) echo "Sync complete... <br />";
			
			if ($deleteFile) {
				if ($debug) echo "Deleting file... <br />";
				$deleteFileResult = unlink($filePathAbs);
				if ($debug) echo "File delete status $deleteFileResult <br />";
			}
		}

		else {
			if ($debug) echo "Error: Sync not completed... <br />";
		}




		echo "<pre>";
			print_r($syncedDevices);
		echo "</pre>";
		



		// Fetch all netscanner devices
		$query = "SELECT * FROM msh_devices WHERE binding LIKE 'netscanner'";
		$result = $mysqli->query($query);
		$numRows = $result->num_rows;

		while ($row = $result->fetch_array()) {
			if (in_array($row['device_ext_id'], $syncedDevices)) {
				//echo "This device is synced, {$row['device_name']} - {$row['device_ext_id']}<br />";
			} else {
				//echo "This device is NOT synced, {$row['device_name']} - {$row['device_ext_id']}<br />";

				// Add log
				if (in_array($row['device_name'], $logDevices)) {
					echo "Device away: Log device {$row['device_int_id']} <br />";

					// Update MSH-device
						$params = array (
							"device_int_id" => $row['device_int_id'],
							"state" => 0,
						);

						
						$resultUpdate = $objDevices->updateDevice($params);

					$p = array (
						'device_int_id' => $row['device_int_id'],
						'time' => $syncTime,
						'unit_id' => 4,
						'value' => 0,
					);

					$addLogResult = $objDevices->addLog($p);
					echo "<pre>";
						print_r($addLogResult);
					echo "</pre>";	
				}
			}


			// Delete old log
			if (!empty($row['device_int_id'])) {
				$timeBack = (time() - 864000); // 864000 = 10 days
				$query2 = "DELETE FROM msh_devices_log WHERE device_int_id='".$row['device_int_id']."' AND time < '$timeBack'";
				$result2 = $mysqli->query($query2);
			}

		} //end-while
	}



	// Die if file not found
	else {
		die('File not found');
	}
	
?>