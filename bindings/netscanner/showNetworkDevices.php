<?php
	
	$query = "SELECT * FROM msh_binding_netscanner ORDER BY ip_adress ASC";
	$result = $mysqli->query($query);
	$numRows = $result->num_rows;

	echo "<table class='table table-striped table-hover'>";

	echo "<thead>";
		echo "<tr>";
			echo "<th>"._('IP-adress')."</th>";
			echo "<th>"._('Hostname')."</th>";
			echo "<th>"._('Hostname edited')."</th>";
			echo "<th>"._('MAC adress')."</th>";
			echo "<th>"._('Last seen')."</th>";
			echo "<th>"._('Last synced')."</th>";
		echo "</tr>";
	echo "</thead>";


	echo "<tbody>";

	while ($row = $result->fetch_array()) {

		echo "<tr>";

			$lastSeen_sec = (time() - $row['last_seen']);

			if ($lastSeen_sec < 240) $active = true;
			else $active = false;


			echo "<td>";

				if ($active) echo "<img style='width:18px; margin-right:8px;' src='core/images/icons/bullet-green.png' alt='active' />";
				else echo "<img style='width:18px; margin-right:8px;' src='core/images/icons/bullet-red.png' alt='inactive' />";

				echo "{$row['ip_adress']}";
			echo "</td>";

			echo "<td>";
				echo "{$row['hostname']}";
			echo "</td>";

			echo "<td>";
				echo "{$row['hostname_edited']}";
			echo "</td>";

			echo "<td>";
				echo "{$row['mac_adress']}";
			echo "</td>";

			echo "<td>";
				echo date('d-m-Y H:i', $row['last_seen']) . ' &nbsp; (' . ago($row['last_seen']) . ')';
			echo "</td>";

			echo "<td>";
				echo date('d-m-Y H:i', $row['last_synced']) . ' &nbsp; (' . ago($row['last_synced']) . ')';
			echo "</td>";

		echo "</tr>";
	}
	echo "</tbody>";

	echo "</table>";

?>