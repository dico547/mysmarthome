<?php
	require_once BINDINGS_PATH . 'netatmo/config.php';
	$userData = $obj->getUserData();
?>

<?php if (!$userData || empty($userData['username'])): ?>
	Please enter your username and password for Netatmo to get started...<br />

	<div style="max-width:300px;">
		<form action="<?php echo BINDINGS_URL; ?>netatmo/Msh_Netatmo.handler.php?action=saveUserCredentials" method="POST">
			<div class="form-group">
				<label for="inputNetatmoMail">Email address</label>
				<input type="email" class="form-control" name="inputNetatmoMail" id="inputNetatmoMail" placeholder="Enter email">
			</div>

			<div class="form-group">
				<label for="inputNetatmoPassword">Password</label>
				<input type="password" class="form-control" name="inputNetatmoPassword" id="inputNetatmoPassword" placeholder="Password">
			</div>

			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>


	<a href="'.BINDINGS_URL.'netatmo/auth2.php?start=true">Click here to authenticate ('.BINDINGS_URL.'netatmo/auth2.php)</a><br />

<?php else: ?>

	<a href="<?php echo BINDINGS_URL; ?>netatmo/sync.php">Synk</a>


	<?php
		$scope = NAScopes::SCOPE_READ_STATION;
		$client = new NAApiClient(array("client_id" => $userData['client_id'], "client_secret" => $userData['client_secret'], "username" => $userData['username'], "password" => $userData['password'], "scope" => $scope));
		$helper = new NAApiHelper($client);
		try {
		    $tokens = $client->getAccessToken();
		} catch(NAClientException $ex) {
		    echo "An error happend while trying to retrieve your tokens\n";
		    exit(-1);
		}
		// Retrieve User Info :
		$user = $helper->api("getuser", "POST");
		echo ("-------------\n");
		echo ("- User Info -\n");
		echo ("-------------\n");
		echo "<pre>";
			print_r($user);
		echo "</pre>";
		echo ("OK\n");
		
		echo ("---------------\n");
		echo ("- Device List -\n");
		echo ("---------------\n");
		$devicelist = $helper->simplifyDeviceList();
		echo "<pre>";
			print_r($devicelist);
		echo "</pre>";
		echo ("OK\n");
		
		echo ("-----------------\n");
		echo ("- Last Measures -\n");
		echo ("-----------------\n");
		$mesures = $helper->getLastMeasures();
		echo "<pre>";
			print_r($mesures);
		echo "</pre>";
		echo ("OK\n");

		echo ("---------------------\n");
		echo ("- Last Day Measures -\n");
		echo ("---------------------\n");
		$mesures = $helper->getAllMeasures(mktime() - 86400);
		echo "<pre>";
			print_r($mesures);
		echo "</pre>";
		echo ("OK\n");
	?>
	
<?php endif; ?>