<?php
	require_once( dirname(__FILE__) . '/config.php' );
	$debug = false;

	// Get and loop all users
	$getUsers = $objUsers->getUsers();
	foreach ($getUsers as $userID => $u) {

	
		// Get netatmo userdata
		$userData = $obj->getUserData($userID);
		
		// Run if user has settings
		if ($userData) {
			if ($debug) echo "Starting sync for user: {$userData['username']} <br />";

			// Auth
			$scope = NAScopes::SCOPE_READ_STATION;
			$client = new NAApiClient(array("client_id" => $userData['client_id'], "client_secret" => $userData['client_secret'], "username" => $userData['username'], "password" => $userData['password'], "scope" => $scope));
			$helper = new NAApiHelper($client);
			try {
			    $tokens = $client->getAccessToken();
			} catch(NAClientException $ex) {
			    echo "An error happend while trying to retrieve your tokens\n";
			    exit(-1);
			}


			// Request devices
			$user = $helper->api("getuser", "POST"); // Retrieve User Info 
			$devicelist = $helper->simplifyDeviceList();
			//$mesures = $helper->getLastMeasures();
			//$mesures = $helper->getAllMeasures(mktime() - 86400);


			if ($debug) {
				/*echo "Device list<br />";

				echo "<pre>";
					print_r($devicelist);
				echo "</pre>";*/
			}


			/*
				Loop devices
				As main unit has sub-units, we build a new array to process
			*/
			foreach ($devicelist['devices'] as $key => $dData) {
				$deviceID = trim($dData['_id']);

				$d[$deviceID]['extID'] = $deviceID;
				$d[$deviceID]['name'] = trim($dData['module_name']);
				$d[$deviceID]['latitude'] = trim($dData['place']['location'][0]);
				$d[$deviceID]['longitude'] = trim($dData['place']['location'][1]);

				$d[$deviceID]['values']['time_read'] = trim($dData['dashboard_data']['time_utc']);
				$d[$deviceID]['values']['temperature'] = trim($dData['dashboard_data']['Temperature']);
				$d[$deviceID]['values']['noise'] = trim($dData['dashboard_data']['Noise']);
				$d[$deviceID]['values']['humidity'] = trim($dData['dashboard_data']['Humidity']);
				$d[$deviceID]['values']['pressure'] = trim($dData['dashboard_data']['Pressure']);
				$d[$deviceID]['values']['CO2'] = trim($dData['dashboard_data']['CO2']);

				// Loop submodules of main unit
				foreach ($dData['modules'] as $key2 => $dModules) {
					$moduleID = trim($dModules['_id']);

					$d[$moduleID]['extID'] = $moduleID;
					$d[$moduleID]['name'] = trim($dModules['module_name']);
					$d[$moduleID]['battery'] = trim($dModules['battery_vp']);

					$d[$moduleID]['values']['time_read'] = trim($dModules['dashboard_data']['time_utc']);
					
					if (isset($dModules['dashboard_data']['Temperature']))
						$d[$moduleID]['values']['temperature'] = trim($dModules['dashboard_data']['Temperature']);
					
					if (isset($dModules['dashboard_data']['Humidity']))
						$d[$moduleID]['values']['humidity'] = trim($dModules['dashboard_data']['Humidity']);
					
					if (isset($dModules['dashboard_data']['CO2']))
						$d[$moduleID]['values']['CO2'] = trim($dModules['dashboard_data']['CO2']);
				}

			}


			if ($debug) {
				echo "Device list (filtered)<br />\n";

				echo "<pre>";
					print_r($d);
				echo "</pre>";
			}

			


			/*
				Loop builded array with devices
				1. Insert/Update devices
				2. Add sensor-data to log
			*/

			foreach ($d as $extID => $nDevice) {
				
				// Check if device exist
				$query = "SELECT * FROM msh_devices WHERE binding LIKE 'netatmo' AND user_id='$userID' AND device_ext_id LIKE '$extID'";
				if ($debug) echo "Checking for existing device. Query: $query<br />\n";
				$result = $mysqli->query($query);
				$numRows = $result->num_rows;


				if ($numRows == 0) // Not exist, create new device
				{
					$p = array (
						'user_id' => $userID,
						'binding' => 'netatmo',
						'device_ext_id' => $extID,
						'device_name' => $nDevice['name'],
						'latitude' => $nDevice['latitude'],
						'longitude' => $nDevice['longitude'],
						'battery' => $nDevice['battery'],
						'monitor' => 1,
					);

					$device_int_id = $objDevices->addDevice($p);
					if ($debug) echo "Created new device. Name: {$nDevice['name']}. ID: $device_int_id<br />\n";


					// Add category to device
					$query = "INSERT INTO msh_devices_has_category SET 
								device_int_id='". $device_int_id ."', 
								category_id='2'";
					$result = $mysqli->query($query);
					
				}

				else // Device exist, update parameters 
				{
					$thisDevice = $result->fetch_array(); //Get existing device data
					$device_int_id = $thisDevice['device_int_id']; // Get current Int ID

					$p = array (
						'device_int_id' => $device_int_id,
						'binding' => 'netatmo',
						'device_ext_id' => $extID,
						'device_name' => $nDevice['name'],
						'latitude' => $nDevice['latitude'],
						'longitude' => $nDevice['longitude'],
						'battery' => $nDevice['battery'],
					);
					$result = $objDevices->updateDevice($p);
					if ($debug) echo "Updated existing device. Name: {$nDevice['name']}. ID: $device_int_id<br />\n";
				}


				// Set method

				// DEPRICATED
				$query = "REPLACE INTO msh_devices_methods SET 
							device_int_id='". $device_int_id ."', 
							method='value', 
							value='". 0 ."'";
				$result = $mysqli->query($query);

				// SET NEW METHOD
				$setMethod[] = array(
					'method_id' => 5,
					'value'=>0
				);

				$numMethods = count($setMethod);
				if ($numMethods > 0) {
					foreach ($setMethod as $key => $mData) {
						if (!empty($mData['method_id'])) {
							$query = "REPLACE INTO msh_devices_has_methods SET 
										d_id='". $device_int_id ."', 
										m_id='". $mData['method_id'] ."'";
							$result = $mysqli->query($query);
						}
					}
				}

				



				// Add log
				if ($thisDevice['monitor'] == 1) // Only log if device should be monitored
				{

					/*
						Loop all sensor data
						Array like
							temperature => value,
							noise => value
							...
					*/
					foreach ($nDevice['values'] as $key => $vData) {
						$error = false;

						if (empty($device_int_id)) {
							$error = true;
						}

						if (empty($nDevice['values']['time_read'])) {
							$error = true;
						}


						// Check for different keys (sensor-types), set correct units for them and set sensor value
						if ($key == 'temperature') {
							$unit_id = 1;
							$sensValue = $nDevice['values']['temperature'];
						}
						elseif ($key == 'noise') {
							$unit_id = 8;
							$sensValue = $nDevice['values']['noise'];
						}
						elseif ($key == 'humidity') {
							$unit_id = 2;
							$sensValue = $nDevice['values']['humidity'];
						}
						elseif ($key == 'pressure') {
							$unit_id = 10;
							$sensValue = $nDevice['values']['pressure'];
						}
						elseif ($key == 'CO2') {
							$unit_id = 9;
							$sensValue = $nDevice['values']['CO2'];
						}
						else {
							$error = true;
						}

						// Replace any , with . for DB
						$sensValue = str_replace(',', '.', $sensValue);


						// Add sensordata to log
						$p = array (
							'device_int_id' => $device_int_id,
							'time' => $nDevice['values']['time_read'],
							'unit_id' => $unit_id,
							'value' => $sensValue,
						);

						if (!$error) {
							$addLogResult = $objDevices->addLog($p);
						} else {
							// ...					
						}

					} //end-foreach value data

				} //end-if monitor

				//echo "<hr />";

			} //end-foreach devices




		} //end-if-userData
	} //end-getUsers()

	
?>
