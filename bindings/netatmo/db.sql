--
-- Tabellstruktur for tabell `msh_bindings_netatmo_auth`
--

CREATE TABLE IF NOT EXISTS `msh_bindings_netatmo_auth` (
  `user_id` int(11) NOT NULL,
  `client_id` varchar(256) NOT NULL,
  `client_secret` varchar(256) NOT NULL,
  `access_token` varchar(256) NOT NULL,
  `refresh_token` varchar(256) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO `msh_cronjobs` (`title`, `filepath`, `interval`, `last_run`, `disabled`) VALUES
('Sync Netatmo values', 'bindings/netatmo/sync.php', 3600, 0, 0);