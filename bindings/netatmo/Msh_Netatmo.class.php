<?php
	
	/**
	* 
	*/
	class MshNetatmo
	{
		
		function __construct()
		{
			# code...
		}


		function getUserData($userID)
		{
			global $mysqli;
			global $thisUser;

			$query = "SELECT * FROM msh_bindings_netatmo_auth WHERE user_id='$userID'";
			$result = $mysqli->query($query);
			$numRows = $result->num_rows;

			if ($numRows > 0) {
				$userData = $result->fetch_array();
				return $userData;
			} else {
				return false;
			}
		}



		function updateUserData($p)
		{
			global $mysqli;
			global $thisUser;

			$r = array();

			echo "<pre>";
				print_r($p);
			echo "</pre>";


			if (!isset($p['user_id'])) $p['user_id'] = $thisUser['user_id'];
			if (empty($p['user_id'])) {
				$r['status'] = 'error';
				$r['message'] = 'User ID is empty';
				return $r;
			}
			

			// Get current userdata
			$userData = $this->getUserData($p['user_id']);

			// Insert user ID to table if not exist
			if (!$userData) {
				$query = "INSERT INTO msh_bindings_netatmo_auth SET user_id='".$p['user_id']."'";
				$result = $mysqli->query($query);

				$r['userdata_inserted'] = $result;
			}

			// Get and set parameters
			if (!isset($p['client_id'])) $p['client_id'] = $userData['client_id'];
			if (!isset($p['client_secret'])) $p['client_secret'] = $userData['client_secret'];
			if (!isset($p['access_token'])) $p['access_token'] = $userData['access_token'];
			if (!isset($p['refresh_token'])) $p['refresh_token'] = $userData['refresh_token'];
			if (!isset($p['username'])) $p['username'] = $userData['username'];
			if (!isset($p['password'])) $p['password'] = $userData['password'];

			// Get global values if empty / not defined anywhere else
			if (empty($p['client_id'])) {
				global $client_id;
				$p['client_id'] = $client_id;
			}

			if (empty($p['client_secret'])) {
				global $client_secret;
				$p['client_secret'] = $client_secret;
			}


			$query = "UPDATE msh_bindings_netatmo_auth SET 
						client_id='".$p['client_id']."', 
						client_secret='".$p['client_secret']."', 
						access_token='".$p['access_token']."', 
						refresh_token='".$p['refresh_token']."', 
						username='".$p['username']."', 
						password='".$p['password']."'
						WHERE user_id='".$p['user_id']."'";
			$result = $mysqli->query($query);

			if ($result) {
				$r['status'] = 'success';
			} else {
				$r['status'] = 'error';
				$r['message'] = 'DB Error';
			}

			return $r;
		}
	}

?>