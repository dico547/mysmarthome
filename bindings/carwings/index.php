<?php

	/* Get userdata
	--------------------------------------------------------------------------- */
	$query = "SELECT * FROM msh_binding_carwings WHERE user_id='{$thisUser['user_id']}'";
	$result = $mysqli->query($query);
	$numRows = $result->num_rows;
	$carwingsData = $result->fetch_array();



	// FORM
	echo "<form class='form-horizontal' role='form' action='msh-bindings/carwings/execute.php?action=saveConfig' method='POST'>";
		

		

		echo "<div class='form-group'>";
			echo "<div class='col-sm-2 control-label'></div>";
			echo "<div class='col-sm-10'>";
				echo "<p>"._('Use the same login information as in mobile application!')."</p>";
			echo "</div>";
		echo "</div>";


		// Username
		echo "<div class='form-group'>";
			echo "<label for='inputCarwingsUsername' class='col-sm-2 control-label'>"._('Username')."</label>";
			echo "<div class='col-sm-10'>";
				echo "<input class='form-control' style='max-width:400px;' type='text' id='inputCarwingsUsername' name='inputCarwingsUsername' placeholder='"._('Carwings username')."' value='{$carwingsData['username']}' />";
			echo "</div>";
		echo "</div>";



		// Password
		echo "<div class='form-group'>";
			echo "<label for='inputCarwingsPassword' class='col-sm-2 control-label'>"._('Password')."</label>";
			echo "<div class='col-sm-10'>";
				echo "<input class='form-control' style='max-width:400px;' type='text' id='inputCarwingsPassword' name='inputCarwingsPassword' placeholder='"._('Carwings password')."' value='{$carwingsData['password']}' />";
			echo "</div>";
		echo "</div>";



		echo "<div class='form-group'>";
			echo "<div class='col-sm-offset-2 col-sm-10'>";
				
				echo "<input class='btn btn-primary' type='submit' name='submit' value='"._('Save')."' />";

			echo "</div>";
		echo "</div>";

	echo "</form>";

?>