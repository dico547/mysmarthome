<?php
	if (isset($_GET['ajax']) && $_GET['ajax'] == true) {
		require_once( dirname(__FILE__) . '/../../core.php' );
	}
?>

<style>
.battery {
  float: left;
  width: 90px;
  height: 160px;
  border: 7px solid #323732;
  border-radius: 10px;
  position: relative;
  margin: 0;
  background-color: #fdfdfd;
}

.battery:before {
  content: '';
  display: block;
  position: absolute;
  top: -16px;
  left: 0;
  right: 0;
  margin: 0 auto;
  width: 40px;
  height: 10px;
  background: #323732;
  border-radius: 2px 2px 0 0;
}

.power {
  position: absolute;
  z-index: 2;
  left: 4px;
  top: 4px;
  right: 4px;
  bottom: 4px;
  background: #ddd;
  border-radius: 4px;
}

.level{
  position: absolute;
  top: 80%;
  left: 0;
  right: 0;
  bottom: 0;
  background: #15dd15;
  border-radius: 4px;
  -webkit-transition: top 0.4s;
  -moz-transition: top 0.4s;
  transition: top 0.4s;
  text-align: center;
}

</style>


<?php
	include(BINDINGS_PATH . '/carwings/carwings.class.php');


	// Carwings
	$resultConfig = $mysqli->query("SELECT * FROM msh_binding_carwings WHERE user_id='{$thisUser['user_id']}' AND car_id='$carID'");
	$carwingsUser = $resultConfig->fetch_array();


	// Populate these with your Carwings username and password
	$username = $carwingsUser['username'];
	$password = $carwingsUser['password'];


	$carwings = new rPHPCarwings($username, $password);




	echo '<h2>' . _('Carwings') . '</h2>';

	echo "<div style='margin-bottom:15px;'>";
		echo "Connected user: $username";
	echo "</div>";



	
	echo "<div style='margin-top:20px;'>";
		echo "<a class='btn btn-success' style='margin:2px;' href='".BINDINGS_URL."carwings/update.php?carID=$carID'>"._('Update')."</a>";
	echo "</div>";
	

	echo "<div class='clearfix'></div>";



	$response_login = $carwings->login();
	$response_info = $carwings->GetVechicleInfo();

	$r['last_update'] = $response_login['OperationDateAndTime'];
	$r['last_update_timestamp'] = strtotime($response_login['OperationDateAndTime']);

	$r['plugin_state'] = $response_login['PluginState'];

	$r['battery']['last_check'] = $response_login['lastBatteryStatusCheckExecutionTime'];
	$r['battery']['last_check_timestamp'] = strtotime($response_login['lastBatteryStatusCheckExecutionTime']);
	$r['battery']['capacity'] = $response_login['BatteryCapacity'];
	$r['battery']['remaining'] = $response_login['BatteryRemainingAmount'];
	$r['battery']['percent'] = ($r['battery']['remaining'] / $r['battery']['capacity']) * 100;
	$r['battery']['charging_status'] = $response_login['BatteryChargingStatus'];

	$r['range']['ac_on'] = $response_login['CruisingRangeAcOn'];
	$r['range']['ac_off'] = $response_login['CruisingRangeAcOff'];
?>


<div style="margin-top:50px;">
	<div class="battery">
		<div class="power">
			<div class="level" style='top:<?php echo (100 - $r['battery']['percent']); ?>%'><?php echo round($r['battery']['percent']); ?>%</div>
		</div>
	</div>
</div>





<?php
	
	echo "<div style='margin-left:120px;'>";

		if ($r['plugin_state'] == 'CONNECTED') {
			echo '<i class="fa fa-plug"></i> ' . _('Charging') . '<br /><br />';
		}

		echo '<b>'._('Last update') .':</b><br />';
		echo date('d-m-Y H:i', $r['last_update_timestamp']) . ' ('.ago($r['last_update_timestamp']).')<br /><br />';

		echo _('Range').': ';
		echo "<b>" . round($r['range']['ac_off'] / 1000) . " km</b><br />";

		echo _('Range').' ('._('AC').' '._('On').'): ';
		echo "<b>" . round($r['range']['ac_on'] / 1000) . " km</b><br />";

	echo "</div>";



	echo "<div class='clearfix'></div>";

	echo "<br /><br />";

	/*echo "<pre>";
		print_r($response_login);
	echo "</pre>";

	echo "<pre>";
		print_r($r);
	echo "</pre>";*/

?>