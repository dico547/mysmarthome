<?php
	
	/* Get config and bindings
	--------------------------------------------------------------------------- */
	require_once( dirname(__FILE__) . '/../core.php' );


	/* Get parameters
	--------------------------------------------------------------------------- */
	if (isset($_GET['id'])) $getID = clean($_GET['id']);
	if (isset($_GET['action'])) $action = clean($_GET['action']);



	/* Save config
	--------------------------------------------------------------------------- */
	if ($action == "saveConfig") {

		$input['username'] 	= clean($_POST['inputCarwingsUsername']);
		$input['password'] 	= clean($_POST['inputCarwingsPassword']);

		$query = "REPLACE INTO msh_binding_carwings SET 
					user_id='".$user['user_id']."', 
					username='".$input['username']."', 
					password='".$input['password']."'";
		$result = $mysqli->query($query);

		header("Location: ".$_SERVER['HTTP_REFERER']."");
		exit();
	}

?>